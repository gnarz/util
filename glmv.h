/* glmv.h
 * 
 * Gunnar Zötl, gz@tset.de, 2013, 2014
 * 
 * header-only lib to do openGL Matrix / Vector math operations
 * 
 * specific to OpenGl insofar as we use the same column-major matrix
 * format OpenGl uses.
 *
 * Default number type is float. If you want to change that, #define
 * GLMV_NUMBER to the number type you wish before including this.
 * 
 * Default function for computing absolute values is fabsf for float,
 * fabs for double and fabsl for everything else (long double). If you
 * want to change that, #define GLMV_ABS to a suitable function name
 * before including this.
 * 
 * Default epsilon for comparison functions is 0. If you want to change
 * that, #define GLMV_EPSILON to a suitable value before inclusing this.
 * 
 * Data types:
 * 	glmv_vec2	a 2-dimensional vector, components x, y
 * 	glmv_vec3	a 3-dimensional vector, components x, y, z
 * 	glmv_vec4	a 4-dimensional vector, components x, y, z, w
 * 	glmv_matrix	a transformation matrix as used by OpenGL, using the same
 * 				column-major format as OpenGl does.
 * 
 * Needed #includes:
 * -----------------
 * 
 * 	<math.h> <stdlib.h>
 * 
 * Functions:
 * ----------
 * 
 *	glmv_vec2 *glmv_vec2_new(GLMV_NUMBER a, GLMV_NUMBER b)
 * 		create a new 2-dimensional vector and initializes it with the
 * 		values a and b.
 * 		Returns the new vector, which can be released using free().
 * 
 *  glmv_vec3 *glmv_vec3_new(GLMV_NUMBER a, GLMV_NUMBER b, GLMV_NUMBER c)
 * 		create a new 3-dimensional vector and initializes it with the
 * 		values a, b and c.
 * 		Returns the new vector, which can be released using free().
 * 
 *	glmv_vec4 *glmv_vec4_new(GLMV_NUMBER a, GLMV_NUMBER b, GLMV_NUMBER c, GLMV_NUMBER d)
 * 		create a new 4-dimensional vector and initializes it with the
 * 		values a, b, c and d.
 * 		Returns the new vector, which can be released using free().
 * 
 * for ? = 2, 3, 4:
 * 
 *	int glmv_vec?_eq(glmv_vec? *v1, glmv_vec? *v2)
 * 		componentwise compares v1 and v2, returns 1 if equal, 0 if not.
 * 		equal, for this function, means that the difference between the
 * 		values is less than or equal to GLMV_EPSILON.
 * 
 *	int glmv_vec?_ne(glmv_vec? *v1, glmv_vec? *v2)
 * 		componentwise compares v1 and v2, returns 0 if equal, 1 if not.
 * 		equal, for this function, means that the difference between the
 * 		values is less than or equal to GLMV_EPSILON.
 *
 *	glmv_vec? *glmv_vec?_add(glmv_vec? *v1, glmv_vec? *v2, glmv_vec? *res)
 * 		adds v1 to v2, placing the result in res.
 * 		Returns res.
 * 
 *	glmv_vec? *glmv_vec?_sub(glmv_vec? *v1, glmv_vec? *v2, glmv_vec? *res)
 * 		subtracts v2 from v1, placing the result in res.
 * 		Returns res.
 * 
 * 	glmv_vec? *glmv_vec?_neg(glmv_vec? *v, glmv_vec? *res)
 * 		negates v, placing the result in res.
 * 		Returns res
 * 
 *	glmv_vec? *glmv_vec?_mul(glmv_vec? *v, GLMV_NUMBER n, glmv_vec? *res)
 * 		multiplies v by the scalar n, placing the result in res.
 * 		Returns res.
 * 
 *	glmv_vec? *glmv_vec?_div(glmv_vec? *v, GLMV_NUMBER n, glmv_vec? *res)
 * 		divides v by scalar n, placing the result in res.
 * 		Returns res.
 * 
 *	GLMV_NUMBER glmv_vec?_dot(glmv_vec? *v1, glmv_vec? *v2)
 * 		Returns the dot product of v1 and v2.
 * 
 *	GLMV_NUMBER glmv_vec?_len(glmv_vec? *v)
 * 		Returns the length of v.
 * 
 *	glmv_vec? *glmv_vec?_normalize(glmv_vec? *v, glmv_vec? *res)
 * 		Normalizes v and places the result in res.
 * 		Returns res.
 * 
 * 	glmv_vec3 *glmv_vec3_cross(glmv_vec3 *v1, glmv_vec3 *v2, glmv_vec3 *res)
 * 		Returns the cross product of v1 and v2
 * 
 *	glmv_matrix *glmv_matrix_new(void)
 * 		creates a new transformation matrix, and initializes it with the
 * 		identity matrix.
 *	 	Returns the new matrix, which can be released using free().
 * 
 *	glmv_matrix *glmv_matrix_loadidentity(glmv_matrix *m)
 * 		loads the identity matrix into m.
 * 		Returns m.
 * 
 * 	glmv_matrix *glmv_matrix_load(glmv_matrix *m, GLMV_NUMBER *n)
 * 		loads the values from the GLMV_NUMBER array n into m.
 * 		Returns m.
 * 
 * 	glmv_matrix *glmv_matrix_loadmatrix(glmv_matrix *m, glmv_matrix *m1)
 * 		loads the values from m1 into m.
 * 		Returns m.
 * 
 *	int glmv_matrix_eq(glmv_matrix *m1, glmv_matrix *m2)
 * 		componentwise compares m1 and m2, returns 1 if equal, 0 if not.
 * 		equal, for this function, means that the difference between the
 * 		values is less than or equal to GLMV_EPSILON.
 * 
 *	int glmv_matrix_ne(glmv_matrix *m1, glmv_matrix *m2)
 * 		componentwise compares m1 and m2, returns 0 if equal, 1 if not.
 * 		equal, for this function, means that the difference between the
 * 		values is less than or equal to GLMV_EPSILON.
 * 
 *	glmv_matrix *glmv_matrix_mul(glmv_matrix *m1, glmv_matrix *m2, glmv_matrix *res)
 * 		multiplies 2 matrices, placing the result in res. res must be
 * 		distinct from both m1 and m2.
 * 		Returns res.
 *
 * 	glmv_matrix *glmv_matrix_rotate(glmv_matrix *m, GLMV_NUMBER a, GLMV_NUMBER x, GLMV_NUMBER y, GLMV_NUMBER z)
 * 		multiply m by a 3d rotation matrix, placing the result in m.
 * 		Returns m.
 *
 * 	glmv_matrix *glmv_matrix_translate(glmv_matrix *m, GLMV_NUMBER x, GLMV_NUMBER y, GLMV_NUMBER z)
 * 		multiply m by a 3d translation matrix, placing the result in m.
 * 		Returns m.
 *
 * 	glmv_matrix *glmv_matrix_scale(glmv_matrix *m, GLMV_NUMBER x, GLMV_NUMBER y, GLMV_NUMBER z)
 * 		multiply m by a 3d scale matrix, placing the result in m.
 * 		Returns m.
 *
 *	glmv_matrix *glmv_matrix_transpose(glmv_matrix *m)
 * 		transposes m, places the result into m.
 * 		Returns m.
 * 
 *	glmv_matrix *glmv_matrix_invert(glmv_matrix *m)
 * 		inverts m, if possible.
 * 		Returns m, if an inversion was possible, NULL otherwise.
 * 
 *	glmv_vec2 *glmv_matrix_transform_vec2(glmv_matrix *m, glmv_vec2 *v, glmv_vec2 *res)
 * 		transforms a v by multiplying it to m, placing the result in res.
 * 		res must be distinct from v.
 * 		Returns res.
 * 
 *	glmv_vec3 *glmv_matrix_transform_vec3(glmv_matrix *m, glmv_vec3 *v, glmv_vec3 *res)
 * 		transforms a v by multiplying it to m, placing the result in res.
 * 		res must be distinct from v.
 * 		Returns res.
 * 
 *	glmv_vec4 *glmv_matrix_transform_vec4(glmv_matrix *m, glmv_vec4 *v, glmv_vec4 *res)
 * 		transforms a v by multiplying it to m, placing the result in res.
 * 		res must be distinct from v.
 * 		Returns res.
 * 
 * Debugging aids:
 * ---------------
 * 
 * your including file must include <stdio.h> for this to work.
 * 
 * #ifdef GLMV_DEBUG
 *
 * 	void glmvPrintMatrix(glmv_matrix *m)
 * 	void glmv_print_vec2(glmv_vec2 *v)
 * 	void glmv_print_vec3(glmv_vec3 *v)
 * 	void glmv_print_vec4(glmv_vec4 *v)
 * 		print a formatted representation of the argument to stdout.
 *
 * #endif
 * 
 * Example:
 * --------
 * 
	#include <stdio.h>
	#include <stdlib.h>
	#include <math.h>

	#define GLMV_DEBUG
	#include "glmv.h"

	int main(int argc, char **argv)
	{
		glmv_matrix *tf = glmv_matrix_new();
		glmv_vec3 v1, v2, *v;
		
		v1.x = 1; v1.y = 0; v1.z = 0;
		
		glmv_matrix_rotate(tf, M_PI / 3, 1, 1, 1);
		
		v = glmv_matrix_transform_vec3(tf, &v1, &v2);
		glmv_print_vec3(v);
		
		exit(0);
	}
 *
 * License:
 * --------
 * 
 * Copyright (c) 2014 Gunnar Zötl <gz@tset.de>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef _glmv_h
#define _glmv_h

#ifndef GLMV_NUMBER
	#define GLMV_NUMBER float
#endif

#ifndef GLMV_ABS
	#if GLMV_NUMBER==float
		#define GLMV_ABS fabsf
	#elif GLMV_NUMBER==double
		#define GLMV_ABS fabs
	#else
		#define GLMV_ABS fabsl
	#endif
#endif

#ifndef GLMV_EPSILON
	#define GLMV_EPSILON 0.0
#endif

#define abs(x) GLMV_ABS(x)

typedef struct {
	GLMV_NUMBER x, y;
} glmv_vec2;

static inline glmv_vec2 *glmv_vec2_new(GLMV_NUMBER a, GLMV_NUMBER b)
{
	glmv_vec2 *v = malloc(sizeof(glmv_vec2));
	v->x = a;
	v->y = b;
	return v;
}

static inline int glmv_vec2_eq(glmv_vec2 *v1, glmv_vec2 *v2)
{
	return (abs(v1->x - v2->x) <= GLMV_EPSILON) &&
		(abs(v1->y - v2->y) <= GLMV_EPSILON);
}

static inline int glmv_vec2_ne(glmv_vec2 *v1, glmv_vec2 *v2)
{
	return (abs(v1->x - v2->x) > GLMV_EPSILON) ||
		(abs(v1->y - v2->y) > GLMV_EPSILON);
}

static inline glmv_vec2 *glmv_vec2_add(glmv_vec2 *v1, glmv_vec2 *v2, glmv_vec2 *res)
{
	res->x = v1->x + v2->x;
	res->y = v1->y + v2->y;
	return res;
}

static inline glmv_vec2 *glmv_vec2_sub(glmv_vec2 *v1, glmv_vec2 *v2, glmv_vec2 *res)
{
	res->x = v1->x - v2->x;
	res->y = v1->y - v2->y;
	return res;
}

static inline glmv_vec2 *glmv_vec2_neg(glmv_vec2 *v, glmv_vec2 *res)
{
	res->x = -v->x;
	res->y = -v->y;
	return res;
}

static inline glmv_vec2 *glmv_vec2_mul(glmv_vec2 *v, GLMV_NUMBER n, glmv_vec2 *res)
{
	res->x = v->x * n;
	res->y = v->y * n;
	return res;
}

static inline glmv_vec2 *glmv_vec2_div(glmv_vec2 *v, GLMV_NUMBER n, glmv_vec2 *res)
{
	res->x = v->x / n;
	res->y = v->y / n;
	return res;
}

static inline GLMV_NUMBER glmv_vec2_dot(glmv_vec2 *v1, glmv_vec2 *v2)
{
	return v1->x*v2->x + v1->y*v2->y;
}

static inline GLMV_NUMBER glmv_vec2_len(glmv_vec2 *v)
{
	return sqrt(v->x*v->x + v->y*v->y);
}

static inline glmv_vec2 *glmv_vec2_normalize(glmv_vec2 *v, glmv_vec2 *res)
{
	return glmv_vec2_div(v, glmv_vec2_len(v), res);
}

typedef struct {
	GLMV_NUMBER x, y, z;
} glmv_vec3;

static inline glmv_vec3 *glmv_vec3_new(GLMV_NUMBER a, GLMV_NUMBER b, GLMV_NUMBER c)
{
	glmv_vec3 *v = malloc(sizeof(glmv_vec3));
	v->x = a;
	v->y = b;
	v->z = c;
	return v;
}

static inline int glmv_vec3_eq(glmv_vec3 *v1, glmv_vec3 *v2)
{
	return (abs(v1->x - v2->x) <= GLMV_EPSILON) &&
		(abs(v1->y - v2->y) <= GLMV_EPSILON) &&
		(abs(v1->z - v2->z) <= GLMV_EPSILON);
}

static inline int glmv_vec3_ne(glmv_vec3 *v1, glmv_vec3 *v2)
{
	return (abs(v1->x - v2->x) > GLMV_EPSILON) ||
		(abs(v1->y - v2->y) > GLMV_EPSILON) ||
		(abs(v1->z - v2->z) > GLMV_EPSILON);
}

static inline glmv_vec3 *glmv_vec3_add(glmv_vec3 *v1, glmv_vec3 *v2, glmv_vec3 *res)
{
	res->x = v1->x + v2->x;
	res->y = v1->y + v2->y;
	res->z = v1->z + v2->z;
	return res;
}

static inline glmv_vec3 *glmv_vec3_sub(glmv_vec3 *v1, glmv_vec3 *v2, glmv_vec3 *res)
{
	res->x = v1->x - v2->x;
	res->y = v1->y - v2->y;
	res->z = v1->z - v2->z;
	return res;
}

static inline glmv_vec3 *glmv_vec3_neg(glmv_vec3 *v, glmv_vec3 *res)
{
	res->x = -v->x;
	res->y = -v->y;
	res->z = -v->z;
	return res;
}

static inline glmv_vec3 *glmv_vec3_mul(glmv_vec3 *v, GLMV_NUMBER n, glmv_vec3 *res)
{
	res->x = v->x * n;
	res->y = v->y * n;
	res->z = v->z * n;
	return res;
}

static inline glmv_vec3 *glmv_vec3_div(glmv_vec3 *v, GLMV_NUMBER n, glmv_vec3 *res)
{
	res->x = v->x / n;
	res->y = v->y / n;
	res->z = v->z / n;
	return res;
}

static inline glmv_vec3 *glmv_vec3_cross(glmv_vec3 *v1, glmv_vec3 *v2, glmv_vec3 *res)
{
	res->x = v1->y * v2->z - v1->z * v2->y;
	res->y = v1->z * v2->x - v1->x * v2->z;
	res->z = v1->x * v2->y - v1->y * v2->x;
	return res;
}

static inline GLMV_NUMBER glmv_vec3_dot(glmv_vec3 *v1, glmv_vec3 *v2)
{
	return v1->x*v2->x + v1->y*v2->y + v1->z*v2->z;
}

static inline GLMV_NUMBER glmv_vec3_len(glmv_vec3 *v)
{
	return sqrt(v->x*v->x + v->y*v->y + v->z*v->z);
}

static inline glmv_vec3 *glmv_vec3_normalize(glmv_vec3 *v, glmv_vec3 *res)
{
	return glmv_vec3_div(v, glmv_vec3_len(v), res);
}

typedef struct {
	GLMV_NUMBER x, y, z, w;
} glmv_vec4;

static inline glmv_vec4 *glmv_vec4_new(GLMV_NUMBER a, GLMV_NUMBER b, GLMV_NUMBER c, GLMV_NUMBER d)
{
	glmv_vec4 *v = malloc(sizeof(glmv_vec4));
	v->x = a;
	v->y = b;
	v->z = c;
	v->w = d;
	return v;
}

static inline int glmv_vec4_eq(glmv_vec4 *v1, glmv_vec4 *v2)
{
	return (abs(v1->x - v2->x) <= GLMV_EPSILON) &&
		(abs(v1->y - v2->y) <= GLMV_EPSILON) &&
		(abs(v1->z - v2->z) <= GLMV_EPSILON) &&
		(abs(v1->w - v2->w) <= GLMV_EPSILON);
}

static inline int glmv_vec4_ne(glmv_vec4 *v1, glmv_vec4 *v2)
{
	return (abs(v1->x - v2->x) > GLMV_EPSILON) ||
		(abs(v1->y - v2->y) > GLMV_EPSILON) ||
		(abs(v1->z - v2->z) > GLMV_EPSILON) ||
		(abs(v1->w - v2->w) > GLMV_EPSILON);
}

static inline glmv_vec4 *glmv_vec4_add(glmv_vec4 *v1, glmv_vec4 *v2, glmv_vec4 *res)
{
	res->x = v1->x + v2->x;
	res->y = v1->y + v2->y;
	res->z = v1->z + v2->z;
	res->w = v1->w + v2->w;
	return res;
}

static inline glmv_vec4 *glmv_vec4_sub(glmv_vec4 *v1, glmv_vec4 *v2, glmv_vec4 *res)
{
	res->x = v1->x - v2->x;
	res->y = v1->y - v2->y;
	res->z = v1->z - v2->z;
	res->w = v1->w - v2->w;
	return res;
}

static inline glmv_vec4 *glmv_vec4_neg(glmv_vec4 *v, glmv_vec4 *res)
{
	res->x = -v->x;
	res->y = -v->y;
	res->z = -v->z;
	res->w = -v->w;
	return res;
}

static inline glmv_vec4 *glmv_vec4_mul(glmv_vec4 *v, GLMV_NUMBER n, glmv_vec4 *res)
{
	res->x = v->x * n;
	res->y = v->y * n;
	res->z = v->z * n;
	res->w = v->w * n;
	return res;
}

static inline glmv_vec4 *glmv_vec4_div(glmv_vec4 *v, GLMV_NUMBER n, glmv_vec4 *res)
{
	res->x = v->x / n;
	res->y = v->y / n;
	res->z = v->z / n;
	res->w = v->w / n;
	return res;
}

static inline GLMV_NUMBER glmv_vec4_dot(glmv_vec4 *v1, glmv_vec4 *v2)
{
	return v1->x*v2->x + v1->y*v2->y + v1->z*v2->z + v1->w*v2->w;
}

static inline GLMV_NUMBER glmv_vec4_len(glmv_vec4 *v)
{
	return sqrt(v->x*v->x + v->y*v->y + v->z*v->z + v->w*v->w);
}

static inline glmv_vec4 *glmv_vec4_normalize(glmv_vec4 *v, glmv_vec4 *res)
{
	return glmv_vec4_div(v, glmv_vec4_len(v), res);
}

/* opengl matrix is oriented like this:
 * 0  4  8 12
 * 1  5  9 13
 * 2  6 10 14
 * 3  7 11 15
 */

typedef GLMV_NUMBER glmv_matrix[16];

static inline glmv_matrix *glmv_matrix_loadidentity(glmv_matrix *m)
{
	(*m)[ 0] = 1.0; (*m)[ 4] = 0.0; (*m)[ 8] = 0.0; (*m)[12] = 0.0;
	(*m)[ 1] = 0.0; (*m)[ 5] = 1.0; (*m)[ 9] = 0.0; (*m)[13] = 0.0;  
	(*m)[ 2] = 0.0; (*m)[ 6] = 0.0; (*m)[10] = 1.0; (*m)[14] = 0.0;
	(*m)[ 3] = 0.0; (*m)[ 7] = 0.0; (*m)[11] = 0.0; (*m)[15] = 1.0;
	return m;
}

static inline glmv_matrix *glmv_matrix_load(glmv_matrix *m, GLMV_NUMBER *n)
{
	int i;
	for (i = 0; i < 16; ++i) {
		(*m)[i] = n[i];
	}
	return m;
}

static inline glmv_matrix *glmv_matrix_loadmatrix(glmv_matrix *m, glmv_matrix *m1)
{
	return glmv_matrix_load(m, &((*m1)[0]));
}

static inline glmv_matrix *glmv_matrix_new(void)
{
	glmv_matrix *m = malloc(sizeof(glmv_matrix));
	return glmv_matrix_loadidentity(m);
}

static inline int glmv_matrix_eq(glmv_matrix *m1, glmv_matrix *m2)
{
	int i = 0, eq = 1;
	for (i = 0; eq && (i < 16); ++i)
		eq = abs((*m1)[i] - (*m2)[i]) <= GLMV_EPSILON;
	return eq;
}

static inline int glmv_matrix_ne(glmv_matrix *m1, glmv_matrix *m2)
{
	int i = 0, ne = 0;
	for (i = 0; (!ne) && (i < 16); ++i)
		ne = abs((*m1)[i] - (*m2)[i]) > GLMV_EPSILON;
	return ne;
}

static inline glmv_matrix *glmv_matrix_mul(glmv_matrix *m1, glmv_matrix *m2, glmv_matrix *res)
{
	(*res)[ 0] = (*m1)[ 0]*(*m2)[ 0] + (*m1)[ 4]*(*m2)[ 1] + (*m1)[ 8]*(*m2)[ 2] + (*m1)[12]*(*m2)[ 3];
	(*res)[ 1] = (*m1)[ 1]*(*m2)[ 0] + (*m1)[ 5]*(*m2)[ 1] + (*m1)[ 9]*(*m2)[ 2] + (*m1)[13]*(*m2)[ 3];
	(*res)[ 2] = (*m1)[ 2]*(*m2)[ 0] + (*m1)[ 6]*(*m2)[ 1] + (*m1)[10]*(*m2)[ 2] + (*m1)[14]*(*m2)[ 3];
	(*res)[ 3] = (*m1)[ 3]*(*m2)[ 0] + (*m1)[ 7]*(*m2)[ 1] + (*m1)[11]*(*m2)[ 2] + (*m1)[15]*(*m2)[ 3];
	(*res)[ 4] = (*m1)[ 0]*(*m2)[ 4] + (*m1)[ 4]*(*m2)[ 5] + (*m1)[ 8]*(*m2)[ 6] + (*m1)[12]*(*m2)[ 7];
	(*res)[ 5] = (*m1)[ 1]*(*m2)[ 4] + (*m1)[ 5]*(*m2)[ 5] + (*m1)[ 9]*(*m2)[ 6] + (*m1)[13]*(*m2)[ 7];
	(*res)[ 6] = (*m1)[ 2]*(*m2)[ 4] + (*m1)[ 6]*(*m2)[ 5] + (*m1)[10]*(*m2)[ 6] + (*m1)[14]*(*m2)[ 7];
	(*res)[ 7] = (*m1)[ 3]*(*m2)[ 4] + (*m1)[ 7]*(*m2)[ 5] + (*m1)[11]*(*m2)[ 6] + (*m1)[15]*(*m2)[ 7];
	(*res)[ 8] = (*m1)[ 0]*(*m2)[ 8] + (*m1)[ 4]*(*m2)[ 9] + (*m1)[ 8]*(*m2)[10] + (*m1)[12]*(*m2)[11];
	(*res)[ 9] = (*m1)[ 1]*(*m2)[ 8] + (*m1)[ 5]*(*m2)[ 9] + (*m1)[ 9]*(*m2)[10] + (*m1)[13]*(*m2)[11];
	(*res)[10] = (*m1)[ 2]*(*m2)[ 8] + (*m1)[ 6]*(*m2)[ 9] + (*m1)[10]*(*m2)[10] + (*m1)[14]*(*m2)[11];
	(*res)[11] = (*m1)[ 3]*(*m2)[ 8] + (*m1)[ 7]*(*m2)[ 9] + (*m1)[11]*(*m2)[10] + (*m1)[15]*(*m2)[11];
	(*res)[12] = (*m1)[ 0]*(*m2)[12] + (*m1)[ 4]*(*m2)[13] + (*m1)[ 8]*(*m2)[14] + (*m1)[12]*(*m2)[15];
	(*res)[13] = (*m1)[ 1]*(*m2)[12] + (*m1)[ 5]*(*m2)[13] + (*m1)[ 9]*(*m2)[14] + (*m1)[13]*(*m2)[15];
	(*res)[14] = (*m1)[ 2]*(*m2)[12] + (*m1)[ 6]*(*m2)[13] + (*m1)[10]*(*m2)[14] + (*m1)[14]*(*m2)[15];
	(*res)[15] = (*m1)[ 3]*(*m2)[12] + (*m1)[ 7]*(*m2)[13] + (*m1)[11]*(*m2)[14] + (*m1)[15]*(*m2)[15];
	return res;
}

static inline glmv_matrix *glmv_matrix_rotate(glmv_matrix *m, GLMV_NUMBER a, GLMV_NUMBER x, GLMV_NUMBER y, GLMV_NUMBER z)
{
	// normalize rotation axis vector first
	GLMV_NUMBER l = sqrt(x*x + y*y + z*z);
	x /= l; y /= l; z /= l;
	GLMV_NUMBER s = sin(a);
	GLMV_NUMBER c = cos(a);
	GLMV_NUMBER mc = 1 - c;

	// compute rotation matrix entries
	GLMV_NUMBER rm0 = x*x*mc + c;
	GLMV_NUMBER rm1 = x*y*mc + z*s;
	GLMV_NUMBER rm2 = x*z*mc - y*s;
	GLMV_NUMBER rm4 = x*y*mc - z*s;
	GLMV_NUMBER rm5 = y*y*mc + c;
	GLMV_NUMBER rm6 = y*z*mc + x*s;
	GLMV_NUMBER rm8 = x*z*mc + y*s;
	GLMV_NUMBER rm9 = y*z*mc - x*s;
	GLMV_NUMBER rm10 = z*z*mc + c;

	GLMV_NUMBER m0 = (*m)[0]; (*m)[ 0] = (*m)[ 0]*rm0 + (*m)[ 4]*rm1 + (*m)[ 8]*rm2;
	GLMV_NUMBER m1 = (*m)[1]; (*m)[ 1] = (*m)[ 1]*rm0 + (*m)[ 5]*rm1 + (*m)[ 9]*rm2;
	GLMV_NUMBER m2 = (*m)[2]; (*m)[ 2] = (*m)[ 2]*rm0 + (*m)[ 6]*rm1 + (*m)[10]*rm2;
	GLMV_NUMBER m3 = (*m)[3]; (*m)[ 3] = (*m)[ 3]*rm0 + (*m)[ 7]*rm1 + (*m)[11]*rm2;
	GLMV_NUMBER m4 = (*m)[4]; (*m)[ 4] = m0*rm4 + (*m)[ 4]*rm5 + (*m)[ 8]*rm6;
	GLMV_NUMBER m5 = (*m)[5]; (*m)[ 5] = m1*rm4 + (*m)[ 5]*rm5 + (*m)[ 9]*rm6;
	GLMV_NUMBER m6 = (*m)[6]; (*m)[ 6] = m2*rm4 + (*m)[ 6]*rm5 + (*m)[10]*rm6;
	GLMV_NUMBER m7 = (*m)[7]; (*m)[ 7] = m3*rm4 + (*m)[ 7]*rm5 + (*m)[11]*rm6;
	(*m)[ 8] = m0*rm8 + m4*rm9 + (*m)[ 8]*rm10;
	(*m)[ 9] = m1*rm8 + m5*rm9 + (*m)[ 9]*rm10;
	(*m)[10] = m2*rm8 + m6*rm9 + (*m)[10]*rm10;
	(*m)[11] = m3*rm8 + m7*rm9 + (*m)[11]*rm10;

	return m;
}

static inline glmv_matrix *glmv_matrix_translate(glmv_matrix *m, GLMV_NUMBER x, GLMV_NUMBER y, GLMV_NUMBER z)
{
	(*m)[12] = (*m)[0]*x + (*m)[4]*y + (*m)[8]*z + (*m)[12];
	(*m)[13] = (*m)[1]*x + (*m)[5]*y + (*m)[9]*z + (*m)[13];
	(*m)[14] = (*m)[2]*x + (*m)[6]*y + (*m)[10]*z + (*m)[14];
	(*m)[15] = (*m)[3]*x + (*m)[7]*y + (*m)[11]*z + (*m)[15];
	return m;
}

static inline glmv_matrix *glmv_matrix_scale(glmv_matrix *m, GLMV_NUMBER x, GLMV_NUMBER y, GLMV_NUMBER z)
{
	(*m)[0] = (*m)[0]*x;
	(*m)[1] = (*m)[1]*x;
	(*m)[2] = (*m)[2]*x;
	(*m)[3] = (*m)[3]*x;
	(*m)[4] = (*m)[4]*y;
	(*m)[5] = (*m)[5]*y;
	(*m)[6] = (*m)[6]*y;
	(*m)[7] = (*m)[7]*y;
	(*m)[8] = (*m)[8]*z;
	(*m)[9] = (*m)[9]*z;
	(*m)[10] = (*m)[10]*z;
	(*m)[11] = (*m)[11]*z;
	return m;
}

static inline glmv_matrix *glmv_matrix_transpose(glmv_matrix *m)
{
	GLMV_NUMBER h;
	h = (*m)[1]; (*m)[1] = (*m)[4]; (*m)[4] = h;
	h = (*m)[2]; (*m)[2] = (*m)[8]; (*m)[8] = h;
	h = (*m)[3]; (*m)[3] = (*m)[12]; (*m)[12] = h;
	h = (*m)[6]; (*m)[6] = (*m)[9]; (*m)[9] = h;
	h = (*m)[7]; (*m)[7] = (*m)[13]; (*m)[13] = h;
	h = (*m)[11]; (*m)[11] = (*m)[14]; (*m)[14] = h;
	return m;
}

/* see http://www.geometrictools.com/Documentation/LaplaceExpansionTheorem.pdf */
static inline glmv_matrix *glmv_matrix_invert(glmv_matrix *m)
{
	GLMV_NUMBER s0 = (*m)[ 0]*(*m)[ 5] - (*m)[ 1]*(*m)[ 4];
	GLMV_NUMBER s1 = (*m)[ 0]*(*m)[ 9] - (*m)[ 1]*(*m)[ 8];
	GLMV_NUMBER s2 = (*m)[ 0]*(*m)[13] - (*m)[ 1]*(*m)[12];
	GLMV_NUMBER s3 = (*m)[ 4]*(*m)[ 9] - (*m)[ 5]*(*m)[ 8];
	GLMV_NUMBER s4 = (*m)[ 4]*(*m)[13] - (*m)[ 5]*(*m)[12];
	GLMV_NUMBER s5 = (*m)[ 8]*(*m)[13] - (*m)[ 9]*(*m)[12];
	GLMV_NUMBER c5 = (*m)[10]*(*m)[15] - (*m)[11]*(*m)[14];
	GLMV_NUMBER c4 = (*m)[ 6]*(*m)[15] - (*m)[ 7]*(*m)[14];
	GLMV_NUMBER c3 = (*m)[ 6]*(*m)[11] - (*m)[ 7]*(*m)[10];
	GLMV_NUMBER c2 = (*m)[ 2]*(*m)[15] - (*m)[ 3]*(*m)[14];
	GLMV_NUMBER c1 = (*m)[ 2]*(*m)[11] - (*m)[ 3]*(*m)[10];
	GLMV_NUMBER c0 = (*m)[ 2]*(*m)[ 7] - (*m)[ 3]*(*m)[ 6];

	GLMV_NUMBER det = s0*c5 - s1*c4 + s2*c3 + s3*c2 - s4*c1 + s5*c0;

	if (det == 0) return (void*)0;

	GLMV_NUMBER m0 = (*m)[0];	(*m)[ 0] = ( (*m)[ 5]*c5 - (*m)[ 9]*c4 + (*m)[13]*c3) / det;
	GLMV_NUMBER m4 = (*m)[4];	(*m)[ 4] = (-(*m)[ 4]*c5 + (*m)[ 8]*c4 - (*m)[12]*c3) / det;
	GLMV_NUMBER m8 = (*m)[8];	(*m)[ 8] = ( (*m)[ 7]*s5 - (*m)[11]*s4 + (*m)[15]*s3) / det;
	GLMV_NUMBER m12 = (*m)[12];	(*m)[12] = (-(*m)[ 6]*s5 + (*m)[10]*s4 - (*m)[14]*s3) / det;

	GLMV_NUMBER m1 = (*m)[1];	(*m)[ 1] = (-(*m)[ 1]*c5 + (*m)[ 9]*c2 - (*m)[13]*c1) / det;
	GLMV_NUMBER m5 = (*m)[5];	(*m)[ 5] = ( m0   *c5 - m8   *c2 + m12  *c1) / det;
	GLMV_NUMBER m9 = (*m)[9];	(*m)[ 9] = (-(*m)[ 3]*s5 + (*m)[11]*s2 - (*m)[15]*s1) / det;
	GLMV_NUMBER m13 = (*m)[13];	(*m)[13] = ( (*m)[ 2]*s5 - (*m)[10]*s2 + (*m)[14]*s1) / det;

	GLMV_NUMBER m2 = (*m)[2];	(*m)[ 2] = ( m1   *c4 - m5   *c2 + m13  *c0) / det;
	GLMV_NUMBER m6 = (*m)[6];	(*m)[ 6] = (-m0   *c4 + m4   *c2 - m12  *c0) / det;
	GLMV_NUMBER m10 = (*m)[10];	(*m)[10] = ( (*m)[ 3]*s4 - (*m)[ 7]*s2 + (*m)[15]*s0) / det;
								(*m)[14] = (-m2   *s4 + m6   *s2 - (*m)[14]*s0) / det;

	GLMV_NUMBER m3 = (*m)[3];	(*m)[ 3] = (-m1   *c3 + m5   *c1 - m9   *c0) / det;
	GLMV_NUMBER m7 = (*m)[7];	(*m)[ 7] = ( m0   *c3 - m4   *c1 + m8   *c0) / det;
								(*m)[11] = (-m3   *s3 + m7   *s1 - (*m)[11]*s0) / det;
								(*m)[15] = ( m2   *s3 - m6   *s1 + m10  *s0) / det;
	
	return m;
}

static inline glmv_vec2 *glmv_matrix_transform_vec2(glmv_matrix *m, glmv_vec2 *v, glmv_vec2 *res)
{
	res->x = (*m)[0]*v->x + (*m)[4]*v->y + (*m)[12];
	res->y = (*m)[1]*v->x + (*m)[5]*v->y + (*m)[13];
	return res;
}

static inline glmv_vec3 *glmv_matrix_transform_vec3(glmv_matrix *m, glmv_vec3 *v, glmv_vec3 *res)
{
	res->x = (*m)[0]*v->x + (*m)[4]*v->y + (*m)[8]*v->z  + (*m)[12];
	res->y = (*m)[1]*v->x + (*m)[5]*v->y + (*m)[9]*v->z  + (*m)[13];
	res->z = (*m)[2]*v->x + (*m)[6]*v->y + (*m)[10]*v->z + (*m)[14];
	return res;
}

static inline glmv_vec4 *glmv_matrix_transform_vec4(glmv_matrix *m, glmv_vec4 *v, glmv_vec4 *res)
{
	res->x = (*m)[0]*v->x + (*m)[4]*v->y + (*m)[8]*v->z  + (*m)[12]*v->w;
	res->y = (*m)[1]*v->x + (*m)[5]*v->y + (*m)[9]*v->z  + (*m)[13]*v->w;
	res->z = (*m)[2]*v->x + (*m)[6]*v->y + (*m)[10]*v->z + (*m)[14]*v->w;
	res->w = (*m)[3]*v->x + (*m)[7]*v->y + (*m)[11]*v->z + (*m)[15]*v->w;
	return res;
}

#ifdef GLMV_DEBUG

/* debugging aids */

static inline void glmv_print_matrix(glmv_matrix *m)
{
	puts("+glmv_matrix+----------+----------+----------+");
	printf("| %8g | %8g | %8g | %8g |\n", (*m)[0], (*m)[4], (*m)[8], (*m)[12]);
	puts("+----------+----------+----------+----------+");
	printf("| %8g | %8g | %8g | %8g |\n", (*m)[1], (*m)[5], (*m)[9], (*m)[13]);
	puts("+----------+----------+----------+----------+");
	printf("| %8g | %8g | %8g | %8g |\n", (*m)[2], (*m)[6], (*m)[10], (*m)[14]);
	puts("+----------+----------+----------+----------+");
	printf("| %8g | %8g | %8g | %8g |\n", (*m)[3], (*m)[7], (*m)[11], (*m)[15]);
	puts("+----------+----------+----------+----------+");
}

static inline void glmv_print_vec2(glmv_vec2 *v)
{
	puts("+glmv_vec2----+");
	printf("| x %8g |\n", v->x);
	printf("| y %8g |\n", v->y);
	puts("+------------+");
}

static inline void glmv_print_vec3(glmv_vec3 *v)
{
	puts("+glmv_vec3----+");
	printf("| x %8g |\n", v->x);
	printf("| y %8g |\n", v->y);
	printf("| z %8g |\n", v->z);
	puts("+------------+");
}

static inline void glmv_print_vec4(glmv_vec4 *v)
{
	puts("+glmv_vec4----+");
	printf("| x %8g |\n", v->x);
	printf("| y %8g |\n", v->y);
	printf("| z %8g |\n", v->z);
	printf("| w %8g |\n", v->w);
	puts("+------------+");
}

#endif /* GLMV_DEBUG */

#endif /* _glmv_h */
