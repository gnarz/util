/* glmv.hpp - header-only lib to do openGL Matrix / Vector math operations
 * C++ version of glmv.h
 * 
 * specific to OpenGl insofar as we use the same column-major matrix
 * format OpenGl uses.
 * 
 * Gunnar Zötl, gz@tset.de, 2013-07
 *
 * the number type is a parameter to the vector creation.
 * 
 * Data types:
 * 	glmvVec2<T>	a 2-dimensional vector, elements x, y or s, t
 * 	glmvVec3<T>	a 3-dimensional vector, elements x, y, z or s, t, p or r, g, b
 * 	glmvVec4<T>	a 4-dimensional vector, elements x, y, z, w or s, t, p, q or r, g, b, a
 * 	glmvMatrix<T>	a transformation matrix as used by OpenGL, using the
 * 				same column-major format as OpenGl does.
 * 
 * Vectors:
 * 	all vector types (glmvVec[234]<T>) support these operations:
 * 	vec = glmvVec[234]<T>()
 * 	vec = glmvVec[234]<T>(T x, T y [, T z, [, T w]])
 * 		(last 2 coordinates only for Vec3 or Vec4 types)
 * 	b = vec == vec
 * 	b = vec != vec
 * 	vec = vec (assignment)
 * 	vec = vec + vec
 * 	vec = vec - vec
 * 	vec = -vec
 * 	vec = vec * t
 * 	vec = vec / t
 * 	vec += vec
 * 	vec -= vec
 * 	vec *= t
 * 	vec /= t
 * 	i = vec:size()	returns the number of components in the vector
 * 	vec = vec.dot(vec)
 * 	t = vec.len()
 *	vec =& vec.normalize() normalizes vector then returns it
 * 	T* t = vec.data()
 *  vec3 also supports the cross product (vec3 = vec3.cross(vec3)
 * 	element access by name or index
 *
 * Matrices:
 * 	matrices support these operations:
 * 	mat = glmvMatrix<T>()
 * 	mat = glmvMatrix<T>(T* m_in)
 * 	mat.load(T* m_in)
 *	mat = mat * mat (the only operation to create a new matrix for the result)
 * 	mat.loadIdentity()
 * 	mat.load(T* m_in)	loads the matrix with values from the array
 * 	mat.load(glmvMatrix<T> m_in) loads the matrix with values from the other matrix
 * 	mat.load(glmvMatrix<T>* m_in) same. load only copies the matrix fields, nothing else.
 * 	mat.rotate(a)
 * 	mat.rotate(a, x, y, z)
 * 	mat.translate(x, y, z=0)
 * 	mat.scale(x, y, z=1) or mat.scale(s)
 * 	mat.transpose()
 * 	mat.invert()
 *	mat.size() returns the number of fields in the matrix (=16)
 * 	mat.data() returns a pointer to the actual matrix data
 * 	vec2 = mat.transform(vec2)
 * 	vec3 = mat.transform(vec3)
 * 	vec4 = mat.transform(vec4)
 * 	T* t = mat.data()
 * 	element access by index
 * 
 * 	additionally, for child classes, the matrix class also provides a
 * 	protected *= operator, which modifies the lhs matrix.
 * 
 * 	all operations on the matrix except for the transform* methods and
 * 	the * operator return the matrix upon which the method was called.
 * 
 * Debugging aids:
 * 
 * if GLMV_DEBUG is defined upon including this, all objects have a
 * print() method.
 *
 * License:
 * --------
 * 
 * Copyright (c) 2014 Gunnar Zötl <gz@tset.de>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef glmv_hpp
#define glmv_hpp

#include <cmath>
#ifdef GLMV_DEBUG
#include <cstdio>
#endif

template <typename T> struct glmvVec2 {

	union {
		struct { T x, y; };
		struct { T s, t; };
	};

	glmvVec2<T>(): x(0), y(0) {}
	glmvVec2<T>(const T x0, const T y0): x(x0), y(y0) {};
	const unsigned int size() const { return 2; }
	const T* data() const { return &x; }

	const T &operator[](const unsigned int i) const
	{
		return *(&x + i);
	}
	
	T &operator[](const unsigned int i)
	{
		return *(&x + i);
	}

	bool operator==(const glmvVec2<T> v) const
	{
		return (x == v.x && y == v.y);
	}

	bool operator!=(const glmvVec2<T> v) const
	{
		return (x != v.x || y != v.y);
	}

	glmvVec2<T> operator+(const glmvVec2<T> v) const
	{
		return glmvVec2<T>(x + v.x, y + v.y);
	}

	glmvVec2<T> operator-(const glmvVec2<T> v) const
	{
		return glmvVec2<T>(x - v.x, y - v.y);
	}

	glmvVec2<T> operator-() const
	{
		return glmvVec2<T>(-x, -y);
	}

	glmvVec2<T> operator*(const T n) const
	{
		return glmvVec2<T>(x * n, y * n);
	}
	
	glmvVec2<T> operator/(const T n) const
	{
		return glmvVec2<T>(x / n, y / n);
	}

	glmvVec2<T>& operator+=(const glmvVec2<T> v)
	{
		x += v.x;
		y += v.y;
		return *this;
	}

	glmvVec2<T>& operator-=(const glmvVec2<T> v)
	{
		x -= v.x;
		y -= v.y;
		return *this;
	}

	glmvVec2<T>& operator*=(const T n)
	{
		x *= n;
		y *= n;
		return *this;
	}

	glmvVec2<T>& operator/=(const T n)
	{
		x /= n;
		y /= n;
		return *this;
	}

	T dot(const glmvVec2<T> v) const
	{
		return x * v.x + y * v.y;
	}

	T len() const
	{
		return (T)sqrt(x * x + y * y);
	}

	glmvVec2<T>& normalize()
	{
		T l = len();
		x = x / l;
		y = y / l;
		return *this;
	}

#ifdef GLMV_DEBUG
	void print() const
	{
		printf("glmvVec2(x: %g, y: %g)\n", x, y);
	}
#endif
};

template <typename T> struct glmvVec3 {

	union {
		struct { T x, y, z; };
		struct { T r, g, b; };
		struct { T s, t, p; };
	};

	glmvVec3<T>(): x(0), y(0), z(0) {}
	glmvVec3<T>(const T x0, const T y0, const T z0): x(x0), y(y0), z(z0) {};
	const unsigned int size() const { return 3; }
	const T* data() const { return &x; }

	const T &operator[](const unsigned int i) const
	{
		return *(&x + i);
	}
	
	T &operator[](const unsigned int i)
	{
		return *(&x + i);
	}

	bool operator==(const glmvVec3<T> v)
	{
		return (x == v.x && y == v.y && z == v.z);
	}

	bool operator!=(const glmvVec3<T> v)
	{
		return (x != v.x || y != v.y || z != v.z);
	}

	glmvVec3<T> operator+(const glmvVec3<T> v) const
	{
		return glmvVec3<T>(x + v.x, y + v.y, z + v.z);
	}

	glmvVec3<T> operator-(const glmvVec3<T> v) const
	{
		return glmvVec3<T>(x - v.x, y - v.y, z - v.z);
	}

	glmvVec3<T> operator-() const
	{
		return glmvVec3<T>(-x, -y, -z);
	}

	glmvVec3<T> operator*(const T n) const
	{
		return glmvVec3<T>(x * n, y * n, z * n);
	}

	glmvVec3<T> operator/(const T n) const
	{
		return glmvVec3<T>(x / n, y / n, z / n);
	}

	glmvVec3<T>& operator+=(const glmvVec3<T> v)
	{
		x += v.x;
		y += v.y;
		z += v.z;
		return *this;
	}

	glmvVec3<T>& operator-=(const glmvVec3<T> v)
	{
		x -= v.x;
		y -= v.y;
		z -= v.z;
		return *this;
	}

	glmvVec3<T>& operator*=(const T n)
	{
		x *= n;
		y *= n;
		z *= n;
		return *this;
	}

	glmvVec3<T>& operator/=(const T n)
	{
		x /= n;
		y /= n;
		z /= n;
		return *this;
	}

	T dot(const glmvVec3<T> v) const
	{
		return x * v.x + y * v.y + z * v.z;
	}

	glmvVec3<T> cross(const glmvVec3<T> v) const
	{
		return glmvVec3<T>(y * v.z - z * v.y, z * v.x - x * v.z, x * v.y - y * v.x);
	}

	T len() const
	{
		return (T)sqrt(x * x + y * y + z * z);
	}

	glmvVec3<T>& normalize()
	{
		T l = len();
		x = x / l;
		y = y / l;
		z = z / l;
		return *this;
	}
#ifdef GLMV_DEBUG
	void print() const
	{
		printf("glmvVec3(x: %g, y: %g, z: %g)\n", x, y, z);
	}
#endif
};

template <typename T> struct glmvVec4 {

	union {
		struct { T x, y, z, w; };
		struct { T r, g, b, a; };
		struct { T s, t, p, q; };
	};

	glmvVec4<T>(): x(0), y(0), z(0), w(0) {}
	glmvVec4<T>(const T x0, const T y0, const T z0, const T w0): x(x0), y(y0), z(z0), w(w0) {};
	const unsigned int size() const { return 4; }
	const T* data() const { return &x; }

	const T &operator[](const unsigned int i) const
	{
		return *(&x + i);
	}

	T &operator[](const unsigned int i)
	{
		return *(&x + i);
	}

	bool operator==(const glmvVec4<T> v)
	{
		return (x == v.x && y == v.y && z == v.z && w == v.w);
	}

	bool operator!=(const glmvVec4<T> v)
	{
		return (x != v.x || y != v.y || z != v.z || w != v.w);
	}

	glmvVec4<T> operator+(const glmvVec4<T> v) const
	{
		return glmvVec4<T>(x + v.x, y + v.y, z + v.z, w + v.w);
	}

	glmvVec4<T> operator-(const glmvVec4<T> v) const
	{
		return glmvVec4<T>(x - v.x, y - v.y, z - v.z, w - v.w);
	}

	glmvVec4<T> operator-() const
	{
		return glmvVec4<T>(-x, -y, -z, -w);
	}

	glmvVec4<T> operator*(const T n) const
	{
		return glmvVec4<T>(x * n, y * n, z * n, w * n);
	}

	glmvVec4<T> operator/(const T n) const
	{
		return glmvVec4<T>(x / n, y / n, z / n, w / n);
	}

	glmvVec4<T>& operator+=(const glmvVec4<T> v)
	{
		x += v.x;
		y += v.y;
		z += v.z;
		w += v.w;
		return *this;
	}

	glmvVec4<T>& operator-=(const glmvVec4<T> v)
	{
		x -= v.x;
		y -= v.y;
		z -= v.z;
		w -= v.w;
		return *this;
	}

	glmvVec4<T>& operator*=(const T n)
	{
		x *= n;
		y *= n;
		z *= n;
		w *= n;
		return *this;
	}

	glmvVec4<T>& operator/=(const T n)
	{
		x /= n;
		y /= n;
		z /= n;
		w /= n;
		return *this;
	}

	T dot(const glmvVec4<T> v) const
	{
		return x * v.x + y * v.y + z * v.z + w * v.w;
	}

	T len() const
	{
		return (T)sqrt(x * x + y * y + z * z + w * w);
	}

	glmvVec4<T>& normalize()
	{
		T l = len();
		x = x / l;
		y = y / l;
		z = z / l;
		w = w / l;
		return *this;
	}
#ifdef GLMV_DEBUG
	void print() const
	{
		printf("glmvVec4(x: %g, y: %g, z: %g, w: %g)\n", x, y, z, w);
	}
#endif
};

/* opengl matrix is oriented like this:
 * 0  4  8 12
 * 1  5  9 13
 * 2  6 10 14
 * 3  7 11 15
 */

template <typename T> class glmvMatrix {
	T m[16];

protected:
	glmvMatrix<T> &operator*=(const glmvMatrix<T> m1)
	{
		T res[16];
		res[ 0] = m[ 0]*m1[ 0] + m[ 4]*m1[ 1] + m[ 8]*m1[ 2] + m[12]*m1[ 3];
		res[ 1] = m[ 1]*m1[ 0] + m[ 5]*m1[ 1] + m[ 9]*m1[ 2] + m[13]*m1[ 3];
		res[ 2] = m[ 2]*m1[ 0] + m[ 6]*m1[ 1] + m[10]*m1[ 2] + m[14]*m1[ 3];
		res[ 3] = m[ 3]*m1[ 0] + m[ 7]*m1[ 1] + m[11]*m1[ 2] + m[15]*m1[ 3];
		res[ 4] = m[ 0]*m1[ 4] + m[ 4]*m1[ 5] + m[ 8]*m1[ 6] + m[12]*m1[ 7];
		res[ 5] = m[ 1]*m1[ 4] + m[ 5]*m1[ 5] + m[ 9]*m1[ 6] + m[13]*m1[ 7];
		res[ 6] = m[ 2]*m1[ 4] + m[ 6]*m1[ 5] + m[10]*m1[ 6] + m[14]*m1[ 7];
		res[ 7] = m[ 3]*m1[ 4] + m[ 7]*m1[ 5] + m[11]*m1[ 6] + m[15]*m1[ 7];
		res[ 8] = m[ 0]*m1[ 8] + m[ 4]*m1[ 9] + m[ 8]*m1[10] + m[12]*m1[11];
		res[ 9] = m[ 1]*m1[ 8] + m[ 5]*m1[ 9] + m[ 9]*m1[10] + m[13]*m1[11];
		res[10] = m[ 2]*m1[ 8] + m[ 6]*m1[ 9] + m[10]*m1[10] + m[14]*m1[11];
		res[11] = m[ 3]*m1[ 8] + m[ 7]*m1[ 9] + m[11]*m1[10] + m[15]*m1[11];
		res[12] = m[ 0]*m1[12] + m[ 4]*m1[13] + m[ 8]*m1[14] + m[12]*m1[15];
		res[13] = m[ 1]*m1[12] + m[ 5]*m1[13] + m[ 9]*m1[14] + m[13]*m1[15];
		res[14] = m[ 2]*m1[12] + m[ 6]*m1[13] + m[10]*m1[14] + m[14]*m1[15];
		res[15] = m[ 3]*m1[12] + m[ 7]*m1[13] + m[11]*m1[14] + m[15]*m1[15];
		for (int i = 0; i < 16; ++i)
			m[i] = res[i];
		return *this;
	}

public:
	glmvMatrix<T> &loadIdentity()
	{
		m[ 0] = 1.0; m[ 4] = 0.0; m[ 8] = 0.0; m[12] = 0.0;
		m[ 1] = 0.0; m[ 5] = 1.0; m[ 9] = 0.0; m[13] = 0.0;  
		m[ 2] = 0.0; m[ 6] = 0.0; m[10] = 1.0; m[14] = 0.0;
		m[ 3] = 0.0; m[ 7] = 0.0; m[11] = 0.0; m[15] = 1.0;
		return *this;
	}

	glmvMatrix<T>()
	{
		loadIdentity();
	}

	glmvMatrix<T>(const T* m_in)
	{
		load(m_in);
	}

	const unsigned int size() const { return 16; }
	const T* data() const { return &m[0]; }

	const T &operator[](const unsigned int i) const
	{
		return m[i];
	}
	
	T &operator[](const unsigned int i)
	{
		return m[i];
	}
	
	void load(const T* m_in)
	{
		for (int i = 0; i < 16; ++i)
			m[i] = m_in[i];
	}

	void load(const glmvMatrix<T> m_in)
	{
		load(m_in.m);
	}

	void load(const glmvMatrix<T>* m_in)
	{
		load(m_in->m);
	}

	glmvMatrix<T> operator*(const glmvMatrix<T> m1) const
	{
		glmvMatrix<T> res;
		res[ 0] = m[ 0]*m1[ 0] + m[ 4]*m1[ 1] + m[ 8]*m1[ 2] + m[12]*m1[ 3];
		res[ 1] = m[ 1]*m1[ 0] + m[ 5]*m1[ 1] + m[ 9]*m1[ 2] + m[13]*m1[ 3];
		res[ 2] = m[ 2]*m1[ 0] + m[ 6]*m1[ 1] + m[10]*m1[ 2] + m[14]*m1[ 3];
		res[ 3] = m[ 3]*m1[ 0] + m[ 7]*m1[ 1] + m[11]*m1[ 2] + m[15]*m1[ 3];
		res[ 4] = m[ 0]*m1[ 4] + m[ 4]*m1[ 5] + m[ 8]*m1[ 6] + m[12]*m1[ 7];
		res[ 5] = m[ 1]*m1[ 4] + m[ 5]*m1[ 5] + m[ 9]*m1[ 6] + m[13]*m1[ 7];
		res[ 6] = m[ 2]*m1[ 4] + m[ 6]*m1[ 5] + m[10]*m1[ 6] + m[14]*m1[ 7];
		res[ 7] = m[ 3]*m1[ 4] + m[ 7]*m1[ 5] + m[11]*m1[ 6] + m[15]*m1[ 7];
		res[ 8] = m[ 0]*m1[ 8] + m[ 4]*m1[ 9] + m[ 8]*m1[10] + m[12]*m1[11];
		res[ 9] = m[ 1]*m1[ 8] + m[ 5]*m1[ 9] + m[ 9]*m1[10] + m[13]*m1[11];
		res[10] = m[ 2]*m1[ 8] + m[ 6]*m1[ 9] + m[10]*m1[10] + m[14]*m1[11];
		res[11] = m[ 3]*m1[ 8] + m[ 7]*m1[ 9] + m[11]*m1[10] + m[15]*m1[11];
		res[12] = m[ 0]*m1[12] + m[ 4]*m1[13] + m[ 8]*m1[14] + m[12]*m1[15];
		res[13] = m[ 1]*m1[12] + m[ 5]*m1[13] + m[ 9]*m1[14] + m[13]*m1[15];
		res[14] = m[ 2]*m1[12] + m[ 6]*m1[13] + m[10]*m1[14] + m[14]*m1[15];
		res[15] = m[ 3]*m1[12] + m[ 7]*m1[13] + m[11]*m1[14] + m[15]*m1[15];
		return res;
	}

	glmvMatrix<T> &rotate(const T a)
	{
		T sina = sin(a);
		T cosa = cos(a);
		T m0 = m[0]; m[0] = m[0]*cosa + m[4]*sina;
		T m1 = m[1]; m[1] = m[1]*cosa + m[5]*sina;
		T m2 = m[2]; m[2] = m[2]*cosa + m[6]*sina;
		T m3 = m[3]; m[3] = m[3]*cosa + m[7]*sina;
		m[4] = m0*-sina + m[4]*cosa;
		m[5] = m1*-sina + m[5]*cosa;
		m[6] = m2*-sina + m[6]*cosa;
		m[7] = m3*-sina + m[7]*cosa;
		return *this;
	}

	glmvMatrix<T> &rotate(const T a, const T x, const T y, const T z)
	{
		// normalize rotation axis vector first
		T l = sqrt(x*x + y*y + z*z);
		T lx = x / l, ly = y / l, lz = z / l;
		T s = sin(a);
		T c = cos(a);
		T mc = 1 - c;

		// compute rotation matrix entries
		T rm0 = lx*lx*mc + c;
		T rm1 = lx*ly*mc + lz*s;
		T rm2 = lx*lz*mc - ly*s;
		T rm4 = lx*ly*mc - lz*s;
		T rm5 = ly*ly*mc + c;
		T rm6 = ly*lz*mc + lx*s;
		T rm8 = lx*lz*mc + ly*s;
		T rm9 = ly*lz*mc - lx*s;
		T rm10 = lz*lz*mc + c;

		T m0 = m[0]; m[ 0] = m[ 0]*rm0 + m[ 4]*rm1 + m[ 8]*rm2;
		T m1 = m[1]; m[ 1] = m[ 1]*rm0 + m[ 5]*rm1 + m[ 9]*rm2;
		T m2 = m[2]; m[ 2] = m[ 2]*rm0 + m[ 6]*rm1 + m[10]*rm2;
		T m3 = m[3]; m[ 3] = m[ 3]*rm0 + m[ 7]*rm1 + m[11]*rm2;
		T m4 = m[4]; m[ 4] = m0*rm4 + m[ 4]*rm5 + m[ 8]*rm6;
		T m5 = m[5]; m[ 5] = m1*rm4 + m[ 5]*rm5 + m[ 9]*rm6;
		T m6 = m[6]; m[ 6] = m2*rm4 + m[ 6]*rm5 + m[10]*rm6;
		T m7 = m[7]; m[ 7] = m3*rm4 + m[ 7]*rm5 + m[11]*rm6;
		m[ 8] = m0*rm8 + m4*rm9 + m[ 8]*rm10;
		m[ 9] = m1*rm8 + m5*rm9 + m[ 9]*rm10;
		m[10] = m2*rm8 + m6*rm9 + m[10]*rm10;
		m[11] = m3*rm8 + m7*rm9 + m[11]*rm10;

		return *this;
	}

	glmvMatrix<T> &translate(const T x, const T y, const T z = 0)
	{
		m[12] = m[0]*x + m[4]*y + m[8]*z + m[12];
		m[13] = m[1]*x + m[5]*y + m[9]*z + m[13];
		m[14] = m[2]*x + m[6]*y + m[10]*z + m[14];
		m[15] = m[3]*x + m[7]*y + m[11]*z + m[15];
		return *this;
	}

	glmvMatrix<T> &scale(const T x, const T y, const T z = 1)
	{
		m[0] = m[0]*x;
		m[1] = m[1]*x;
		m[2] = m[2]*x;
		m[3] = m[3]*x;
		m[4] = m[4]*y;
		m[5] = m[5]*y;
		m[6] = m[6]*y;
		m[7] = m[7]*y;
		m[8] = m[8]*z;
		m[9] = m[9]*z;
		m[10] = m[10]*z;
		m[11] = m[11]*z;
		return *this;
	}
	
	glmvMatrix<T> &scale(const T s)
	{
		return scale(s, s, s);
	}

	glmvMatrix<T> &transpose()
	{
		T h;
		h = m[1]; m[1] = m[4]; m[4] = h;
		h = m[2]; m[2] = m[8]; m[8] = h;
		h = m[3]; m[3] = m[12]; m[12] = h;
		h = m[6]; m[6] = m[9]; m[9] = h;
		h = m[7]; m[7] = m[13]; m[13] = h;
		h = m[11]; m[11] = m[14]; m[14] = h;
		return *this;
	}

	bool canInvert()
	{
		T s0 = m[ 0]*m[ 5] - m[ 1]*m[ 4];
		T s1 = m[ 0]*m[ 9] - m[ 1]*m[ 8];
		T s2 = m[ 0]*m[13] - m[ 1]*m[12];
		T s3 = m[ 4]*m[ 9] - m[ 5]*m[ 8];
		T s4 = m[ 4]*m[13] - m[ 5]*m[12];
		T s5 = m[ 8]*m[13] - m[ 9]*m[12];
		T c5 = m[10]*m[15] - m[11]*m[14];
		T c4 = m[ 6]*m[15] - m[ 7]*m[14];
		T c3 = m[ 6]*m[11] - m[ 7]*m[10];
		T c2 = m[ 2]*m[15] - m[ 3]*m[14];
		T c1 = m[ 2]*m[11] - m[ 3]*m[10];
		T c0 = m[ 2]*m[ 7] - m[ 3]*m[ 6];

		T det = s0*c5 - s1*c4 + s2*c3 + s3*c2 - s4*c1 + s5*c0;

		return det != 0;
	}

	/* see http://www.geometrictools.com/Documentation/LaplaceExpansionTheorem.pdf */
	glmvMatrix<T> &invert()
	{
		T s0 = m[ 0]*m[ 5] - m[ 1]*m[ 4];
		T s1 = m[ 0]*m[ 9] - m[ 1]*m[ 8];
		T s2 = m[ 0]*m[13] - m[ 1]*m[12];
		T s3 = m[ 4]*m[ 9] - m[ 5]*m[ 8];
		T s4 = m[ 4]*m[13] - m[ 5]*m[12];
		T s5 = m[ 8]*m[13] - m[ 9]*m[12];
		T c5 = m[10]*m[15] - m[11]*m[14];
		T c4 = m[ 6]*m[15] - m[ 7]*m[14];
		T c3 = m[ 6]*m[11] - m[ 7]*m[10];
		T c2 = m[ 2]*m[15] - m[ 3]*m[14];
		T c1 = m[ 2]*m[11] - m[ 3]*m[10];
		T c0 = m[ 2]*m[ 7] - m[ 3]*m[ 6];

		T det = s0*c5 - s1*c4 + s2*c3 + s3*c2 - s4*c1 + s5*c0;

		T m0 = m[0];	m[ 0] = ( m[ 5]*c5 - m[ 9]*c4 + m[13]*c3) / det;
		T m4 = m[4];	m[ 4] = (-m[ 4]*c5 + m[ 8]*c4 - m[12]*c3) / det;
		T m8 = m[8];	m[ 8] = ( m[ 7]*s5 - m[11]*s4 + m[15]*s3) / det;
		T m12 = m[12];	m[12] = (-m[ 6]*s5 + m[10]*s4 - m[14]*s3) / det;

		T m1 = m[1];	m[ 1] = (-m[ 1]*c5 + m[ 9]*c2 - m[13]*c1) / det;
		T m5 = m[5];	m[ 5] = ( m0   *c5 - m8   *c2 + m12  *c1) / det;
		T m9 = m[9];	m[ 9] = (-m[ 3]*s5 + m[11]*s2 - m[15]*s1) / det;
		T m13 = m[13];	m[13] = ( m[ 2]*s5 - m[10]*s2 + m[14]*s1) / det;

		T m2 = m[2];	m[ 2] = ( m1   *c4 - m5   *c2 + m13  *c0) / det;
		T m6 = m[6];	m[ 6] = (-m0   *c4 + m4   *c2 - m12  *c0) / det;
		T m10 = m[10];	m[10] = ( m[ 3]*s4 - m[ 7]*s2 + m[15]*s0) / det;
		T m14 = m[14];	m[14] = (-m2   *s4 + m6   *s2 - m[14]*s0) / det;

		T m3 = m[3];	m[ 3] = (-m1   *c3 + m5   *c1 - m9   *c0) / det;
		T m7 = m[7];	m[ 7] = ( m0   *c3 - m4   *c1 + m8   *c0) / det;
						m[11] = (-m3   *s3 + m7   *s1 - m[11]*s0) / det;
						m[15] = ( m2   *s3 - m6   *s1 + m10  *s0) / det;
		
		return *this;
	}

	glmvVec2<T> transform(const glmvVec2<T> v) const
	{
		return glmvVec2<T>(
			m[0]*v.x + m[4]*v.y + m[12],
			m[1]*v.x + m[5]*v.y + m[13]);
	}

	glmvVec3<T> transform(const glmvVec3<T> v) const
	{
		return glmvVec3<T>(
			m[0]*v.x + m[4]*v.y + m[8]*v.z  + m[12],
			m[1]*v.x + m[5]*v.y + m[9]*v.z  + m[13],
			m[2]*v.x + m[6]*v.y + m[10]*v.z + m[14]);
	}

	glmvVec4<T> transform(const glmvVec4<T> v) const
	{
		return glmvVec4<T>(
			m[0]*v.x + m[4]*v.y + m[8]*v.z  + m[12]*v.w,
			m[1]*v.x + m[5]*v.y + m[9]*v.z  + m[13]*v.w,
			m[2]*v.x + m[6]*v.y + m[10]*v.z + m[14]*v.w,
			m[3]*v.x + m[7]*v.y + m[11]*v.z + m[15]*v.w);
	}

#ifdef GLMV_DEBUG
	void print()
	{
		puts("glmvMatrix");
		printf("%4g  %4g  %4g  %4g\n", m[0], m[4], m[8], m[12]);
		printf("%4g  %4g  %4g  %4g\n", m[1], m[5], m[9], m[13]);
		printf("%4g  %4g  %4g  %4g\n", m[2], m[6], m[10], m[14]);
		printf("%4g  %4g  %4g  %4g\n", m[3], m[7], m[11], m[15]);
	}
#endif
};

#endif /* glmv_hpp */
