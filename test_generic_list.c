/* gcc -o test_generic_list test_generic_list.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "generic_list.h"

typedef char* charp;
define_list_type(charp);

void charp_free(char **s, void*x)
{
	(void)x;
	if (*s) free(*s);
}

void charp_print(char **s, void*x)
{
	(void)x;
	if (*s) puts(*s);
}

int main(int argc, char** argv)
{
	list(charp) *l = list_new(charp);
	list_iter(charp) *i, *n;
	const char *s;
	
	s = strdup("Hallo"); list_append(l, &s);
	s = strdup("Huhu"); list_append(l, &s);
	s = strdup("Hurz"); list_append(l, &s);
	s = strdup("Hola"); list_append(l, &s);
	s = strdup("Ciao"); list_append(l, &s);
	
	printf("--- First list: length is %lu\n", list_length(l));
	for (i = list_first(l); i != list_end(l); i = list_iter_next(l, i)) {
		printf("List_item sais \"%s\"\n", list_iter_data(l, i));
	}

	s=strdup("insert 1"); list_insert_at(l, 1, &s);
	s=strdup("insert 2"); list_insert_at(l, list_length(l), &s);
	s=strdup("insert 3"); list_insert_at(l, (list_length(l) >> 1) + 1, &s);

	printf("--- Changed list: length is %lu\n", list_length(l));
	list_foreach(l, charp_print, 0);

	i = list_first(l);
	while (i != list_end(l)) {
		n = list_iter_next(l, i);
		s = strdup("x");
		list_insert_after(l, i, &s);
		i = n;
	}

	printf("--- Changed list: length is %lu\n", list_length(l));
	list_foreach(l, charp_print, 0);

	int p;
	for (p = 1; p <= list_length(l); ++p) {
		list_remove_at(l, p);
	}

	printf("--- after remove: length is %lu\n", list_length(l));
	list_foreach(l, charp_print, 0);

	list_foreach(l, charp_free, NULL);
	list_free(l);
	
	exit(1);
}
