/* generic_array.h
 * 
 * Gunnar Zötl <gz@tset.de> 2015
 *
 * Extension for generic_array.h to have sorted and thus easily searchable
 * extensible arrays. Note that these extension functions don't mix well
 * (because of sorting breakage) with
 * - int array_insert(array(T) *a, size_t pos, size_t nelem)
 * - int array_append(array(T) *a, void *data)
 * so don't use those if you use these. Also, be careful if you use
 * - array_resize(array(T) *a, size_t s)
 * - int array_remove(array(T) *a, size_t pos, size_t nelem)
 * (eventhough they do preserve sorting order).
 * 
 * Needed #includes:
 * -----------------
 * 
 * 	<stdlib.h>
 *	<string.h>
 *	"generic_array.h"
 *
 * Type creator:
 * -------------
 *
 *	just create a normal generic_array(T)
 *
 * Functions:
 * ----------
 *
 *	int array_insert_s(array(T) *a, void *elem, _array_cmpfun *cmp)
 *		inserts an element into array at a position p so that
 *		- p == 0 and cmp(elem, a[1]) < 0
 *		- p == array_size(a) - 1 and cmp(a[p-1], elem) < 0
 *		- cmp(a[p-1], elem) < 0 and cmp(elem, a[p+1]) < 0
 *		Will not insert the element, if it is already present.
 *		Returns the index of the inserted element on success, -1 on failure.
 * 
 *	int array_find_or_insert_s(array(T) *a, void *elem, _array_cmpfun *cmp)
 *		inserts an element into array at a position p, just like
 *		array_insert_s() does, but if an element already exists, it will
 *		return its index instead of -1.
 *		Returns the index of the inserted or found element on success,
 *		-1 on failure.
 *
 *	int array_remove_s(array(T) *a, void *elem, _array_cmpfun *cmp)
 *		removes an element from array.
 *		Returns 1 on success, 0 on failure.
 * 
 *	int array_find_s(array(T) *a, void *elem, _array_cmpfun *cmp)
 *		finds an element in an array.
 *		Returns the index of the found element on success, -1 on failure.
 *
 * Notes:
 * ------
 *
 *	cmpfun is an _array_cmpfun, a function with 2 arguments, which are pointers
 *	to the values to be compared, returning an int. The first argument will
 *	always be the element to insert/remove, the second argument a pointer to
 *	the current item from the array to compare against. It must return -1 if
 *	the value pointed to by its first argument is ordered before the value
 *	pointed to by its second argument, 1 if it comes after, or 0 if they are
 *	equal.
 * 
 */

typedef int (*_array_cmpfun)(const void*, const void*);

static inline int _array_insert_s(struct _generic_array *a, void *elem, _array_cmpfun compare, int return_found)
{
	if (!a || !elem || !compare) return -1;

	int left = 0;
	int right = array_size(a) - 1;
	int pos = left;

	while (left <= right) {
		pos = ((right - left) >> 1) + left;
		char *itemp = ((char*)array_data(a)) + pos * a->header.itemsize;
		int cmp = compare(elem, itemp);
		if (cmp < 0) {
			right = pos - 1;
		} else if (cmp > 0) {
			left = pos + 1;
		} else {
			return return_found ? pos : -1;
		}
	}
	array_insert(a, left, 1);
	char *itemp = ((char*)array_data(a)) + left * a->header.itemsize;
	memcpy(itemp, elem, a->header.itemsize);
	return left;
}
#define array_insert_s(a, elem, compare) _array_insert_s((struct _generic_array*)(a), (elem), (compare), 0)
#define array_find_or_insert_s(a, elem, compare) _array_insert_s((struct _generic_array*)(a), (elem), (compare), 1)
 
static inline int _array_find_s(struct _generic_array *a, void *elem, _array_cmpfun compare)
{
	if (!a || !elem || !compare) return -1;

	int left = 0;
	int right = array_size(a) - 1;
	int pos = left;

	while (left <= right) {
		pos = ((right - left) >> 1) + left;
		char *itemp = ((char*)array_data(a)) + pos * a->header.itemsize;
		int cmp = compare(elem, itemp);
		if (cmp < 0) {
			right = pos - 1;
		} else if (cmp > 0) {
			left = pos + 1;
		} else {
			return pos;
		}
	}
	return -1;
}
#define array_find_s(a, elem, compare) _array_find_s((struct _generic_array*)(a), (elem), (compare))

static inline int _array_remove_s(struct _generic_array *a, void *elem, _array_cmpfun compare)
{
	int idx = _array_find_s(a, elem, compare);
	if (idx < 0) return 0;
	return array_remove(a, idx, 1);
}
#define array_remove_s(a, elem, compare) _array_remove_s((struct _generic_array*)(a), (elem), (compare))

