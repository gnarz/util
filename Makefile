# very simple makefile, just builds the tests

CFLAGS = -Wall -g
#CFLAGS = -Wall -Os
LIBS = -lm -lGL -lglut

TEST_SRCS = $(shell echo test_*.c)
TESTS = $(TEST_SRCS:.c=)

all: $(TESTS)

%: %.c
	$(CC) $(CFLAGS) -o $@ $< $(LIBS)

test: $(TESTS)
	for i in $(TESTS); do echo; echo "***" $$i "***"; ./$$i; done

clean:
	rm -f $(TESTS)
