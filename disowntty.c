/* use: exec disowntty
 * compile: gcc -o disowntty disowntty.c
 *
 * give up control of our current controlling terminal. Useful for
 * debugging stuff with gdb -tty
 *
 * found here: https://bugzilla.redhat.com/show_bug.cgi?id=593483#c1
 * and slightly modified.
 */

#include <sys/ioctl.h>
#include <unistd.h>
#include <stdio.h>
#include <limits.h>
#include <stdlib.h>
#include <signal.h>

static void err(const char *msg)
{
	perror(msg);
	puts("did you exec this?");
	exit(1);
}

int main (void)
{
	void (*orig) (int signo);
	setbuf (stdout, NULL);
	orig = signal (SIGHUP, SIG_IGN);
	if (orig != SIG_DFL)
		err("signal (SIGHUP)");
	/* Verify we are the sole owner of the tty.  */
	if (ioctl (STDIN_FILENO, TIOCSCTTY, 0) != 0)
		err("TIOCSCTTY");
	/* output tty name. */
	system("tty");
	/* Disown the tty.  */
	if (ioctl (STDIN_FILENO, TIOCNOTTY) != 0)
		err("TIOCNOTTY");
	puts("OK, disowned. Close terminal when finished.");
	for (;;)
		pause ();
	return 1;
}
