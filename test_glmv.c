// gcc -g -o glmvtest glmvtest.c -lGL -lglut -lm

#include <stdio.h>
#include <string.h>

#include <stdlib.h>
#include <math.h>

#include <GL/gl.h>
#include <GL/glut.h>

#if 1
#define GLMV_NUMBER double
#define glRotatef glRotated
#define glTranslatef glTranslated
#define glMultMatrixf glMultMatrixd
#define glGetFloatv glGetDoublev
#endif

#define GLMV_DEBUG
#define GLMV_EPSILON 0.02
#include "glmv.h"

void printmatrix(const char *title, glmv_matrix *m)
{
	puts(title);
	glmv_print_matrix(m);
	puts("");
}

glmv_matrix glm, m1, m2, m3;

int main(int argc, char **argv)
{
	// need to init opengl in order for the opengl stuff to work...
	glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA);
    glutInitWindowSize(100, 100);
    glutCreateWindow("please ignore...");
    
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glGetFloatv(GL_MODELVIEW_MATRIX, glm);
	//printmatrix("GL identity", &glm);
	
	glmv_matrix_loadidentity(&m1);
	//printmatrix("glmv identity", &m1);
	
	printf("load identity: %s\n", glmv_matrix_eq(&glm, &m1) ? "passed" : "failed");
	
	glMultMatrixf(glm);
	glGetFloatv(GL_MODELVIEW_MATRIX, glm);
	//printmatrix("GL ident*ident", &glm);

	glmv_matrix_loadidentity(&m2);
	glmv_matrix_mul(&m1, &m2, &m3);
	//printmatrix("glmv ident*ident", &m3);
	
	printf("ident*ident: %s\n", glmv_matrix_eq(&glm, &m3) ? "passed" : "failed");
	
	glm[4] = 2; glm[8] = 3; glm[12] = 4; glm[7] = 5;
	glMultMatrixf(glm);
	glGetFloatv(GL_MODELVIEW_MATRIX, glm);
	//printmatrix("GL mult 1", &glm);

	m2[4] = 2; m2[8] = 3; m2[12] = 4; m2[7] = 5;
	glmv_matrix_mul(&m1, &m2, &m3);
	//printmatrix("glmv mult 1", &m3);
	
	printf("mult 1: %s\n", glmv_matrix_eq(&glm, &m3) ? "passed" : "failed");

	glMultMatrixf(glm);
	glGetFloatv(GL_MODELVIEW_MATRIX, glm);
	//printmatrix("GL mult 2", &glm);

	glmv_matrix_mul(&m3, &m2, &m1);
	//printmatrix("glmv mult 2", &m1);
	
	printf("mult 2: %s\n", glmv_matrix_eq(&glm, &m1) ? "passed" : "failed");

	glLoadIdentity();
	glRotatef(180/3, 1, 1, 1);
	glGetFloatv(GL_MODELVIEW_MATRIX, glm);
	//printmatrix("GL rot 3d", &glm);

	glmv_matrix_loadidentity(&m1);
	glmv_matrix_rotate(&m1, M_PI / 3, 1, 1, 1);
	//printmatrix("glmv rot 3d", &m1);
	
	printf("rot 3d: %s\n", glmv_matrix_eq(&glm, &m1) ? "passed" : "failed");

	glRotatef(180/4, 0, 0, 1);
	glGetFloatv(GL_MODELVIEW_MATRIX, glm);
	//printmatrix("GL rot/rot 3d", &glm);

	glmv_matrix_rotate(&m1, M_PI / 4, 0, 0, 1);
	//printmatrix("glmv rot/rot 3d", &m1);
	
	printf("rot / rot 3d: %s\n", glmv_matrix_eq(&glm, &m1) ? "passed" : "failed");

	glLoadIdentity();
	glTranslatef(1, 2, 3);
	glGetFloatv(GL_MODELVIEW_MATRIX, glm);
	//printmatrix("GL trans 3d", &glm);

	glmv_matrix_loadidentity(&m1);
	glmv_matrix_translate(&m1, 1, 2, 3);
	//printmatrix("glmv trans 3d", &m1);
	
	printf("trans 3d: %s\n", glmv_matrix_eq(&glm, &m1) ? "passed" : "failed");
	
	glLoadIdentity();
	glRotatef(180/3, 0, 0, 1);
	glGetFloatv(GL_MODELVIEW_MATRIX, glm);
	//printmatrix("GL rot 2d", &glm);

	glmv_matrix_loadidentity(&m1);
	glmv_matrix_rotate(&m1, M_PI / 3, 0, 0, 1);
	//printmatrix("glmv rot 2d", &m1);
	
	printf("rot 2d: %s\n", glmv_matrix_eq(&glm, &m1) ? "passed" : "failed");

	glLoadIdentity();
	glRotatef(180/3, 1, 1, 1);
	glTranslatef(1.1, 2.2, 3.3);
	glGetFloatv(GL_MODELVIEW_MATRIX, glm);
	//printmatrix("GL rot/trans 3d", &glm);
	
	glmv_matrix_loadidentity(&m1);
	glmv_matrix_rotate(&m1, M_PI/3, 1, 1, 1);
	glmv_matrix_translate(&m1, 1.1, 2.2, 3.3);
	//printmatrix("glmv rot/trans 3d", &m1);
	
	printf("rot/trans 3d: %s\n", glmv_matrix_eq(&glm, &m1) ? "passed" : "failed");

	glLoadIdentity();
	glTranslatef(1, 1, 1);
	glRotatef(180/2, 1, 1, 1);
	glGetFloatv(GL_MODELVIEW_MATRIX, glm);
	//printmatrix("GL trans/rot 3d", &glm);

	glmv_matrix_loadidentity(&m1);
	glmv_matrix_translate(&m1, 1, 1, 1);
	glmv_matrix_rotate(&m1, M_PI/2, 1, 1, 1);
	//printmatrix("glmv trans/rot 3d", &m1);
	
	printf("trans/rot 3d: %s\n", glmv_matrix_eq(&glm, &m1) ? "passed" : "failed");

	glLoadIdentity();
	glRotatef(180/7, 1, 0, 0);
	glTranslatef(1, 0, 1);
	glRotatef(180/7, 0, 1, 0);
	glTranslatef(0, 1, 0);
	glRotatef(180/7, 0, 0, 1);
	glGetFloatv(GL_MODELVIEW_MATRIX, glm);
	//printmatrix("GL rot/trans/rot/trans/rot 3d", &glm);

	glmv_matrix_loadidentity(&m1);
	glmv_matrix_rotate(&m1, M_PI/7, 1, 0, 0);
	glmv_matrix_translate(&m1, 1, 0, 1);
	glmv_matrix_rotate(&m1, M_PI/7, 0, 1, 0);
	glmv_matrix_translate(&m1, 0, 1, 0);
	glmv_matrix_rotate(&m1, M_PI/7, 0, 0, 1);
	//printmatrix("glmv rot/trans/rot/trans/rot 3d", &m1);
	
	printf("rot/trans/rot/trans/rot 3d: %s\n", glmv_matrix_eq(&glm, &m1) ? "passed" : "failed");

	glmv_matrix_loadmatrix(&m2, &m1);
	if (glmv_matrix_invert(&m2)) {
		//printmatrix("glmv invert", &m2);
		glmv_matrix_mul(&m1, &m2, &m3);
		//printmatrix("glmv mat*invert", &m3);
		glmv_matrix_loadidentity(&m1);
		printf("mat * inv(mat): %s\n", glmv_matrix_eq(&m1, &m3) ? "passed" : "failed");
	} else
		puts("Could not invert matrix.");

	exit(0);
}
