/* generic_list.h
 * 
 * Gunnar Zötl <gz@tset.de> 2016
 * 
 * generic single linked lists for C, somewhat type safe. Note: no attempt
 * is made to clean up the data contained in the list elements, you need to
 * do this yourself.
 * 
 * Stuff starting with _list_* is reserved and private. Don't name your
 * identifiers like that, and don't use stuff named like that. Also, in
 * order to achieve some kind of macro hygiene, local variables in macros
 * are named ___lXX, where XX is a number. Don't use those names either.
 *
 * All functions are actually macros, that may call functions. The macros
 * take care of any type conversions needed.
 *
 * Needed #includes:
 * -----------------
 * 
 * 	<stdlib.h> <string.h>
 *
 * Type creator:
 * -------------
 *
 *	define_list_type(T)
 *		creates a struct for a list with element type T
 *		use: define_list(T);
 *			 list(T) the_list = list_new(T);
 * 		Note: Type T must be a single word, so if you have pointers or
 * 		structs, you need to typedef them first.
 *
 * Types:
 * ------
 *
 *	list(T)
 *		type for a list with element type T
 *
 *	list_iter(T)
 *		type for a list(T) iterator
 *
 *	list_iter_of(list(T) *l)
 *		type of the iterator for a given list
 *
 * Functions:
 * ----------
 * 
 *	list(T) *list_new(T)
 *		creates a new list with elements of type T.
 *		Returns the newly created list.
 *
 *	void list_free(list(T) *l)
 *		delete a list.
 *
 *	list_size_t list_length(list(T) *l)
 *		returns the length (number of elements) of a list
 *
 *	int list_append(list(T) *l, const T *v)
 * 		append a value after the last value of a list. v is the
 * 		pointer to the value to append.
 * 		Returns 1 on success, 0 on failure.
 *
 *	int list_prepend(list(T) *l, const T *v)
 * 		insert a value before the first value of a list. v is the
 * 		pointer to the value to insert.
 * 		Returns 1 on success, 0 on failure.
 *
 *	int list_insert_at(list(T) *l, list_size_t p, const T *v)
 * 		insert a value at a given position in a list. v is the
 * 		pointer to the value to insert.
 * 		Returns 1 on success, 0 on failure.
 *
 *	int list_insert_after(list(T) *l, list_iter(T) *i, const T *v)
 * 		insert a value after another value referenced by an iterator in
 *		a list. v is the pointer to the value to insert.
 * 		Returns 1 on success, 0 on failure.
 *
 *	void list_remove_at(list(T) *l, list_size_t p)
 * 		remove the element at position p from the list.
 * 		Returns 1 on success, 0 on failure.
 *
 *	void list_remove(list(T) *l, list_iter(T) *i)
 * 		remove the element areferenced by iterator i from the list.
 * 		Returns 1 on success, 0 on failure.
 *
 *	list_iter(l) *list_item(list(T) *l, list_size_t pos)
 *		returns an iterator to the list element at position pos, or 0
 *		on failure.
 *
 *	list_iter(l) *list_first(list(T) *l)
 *		returns an iterator to first list element, or 0 on failure.
 *
 *	list_iter(l) *list_last(list(T) *l)
 *		returns an iterator to last list element, or 0 on failure.
 *
 *	list_iter(l) *list_end(list(T) *l)
 *		returns an iterator to such that list_next(list_last(l)) == list_end(l),
 *		or 0 on failure.
 *
 *	list_iter(T) *list_iter_next(list(T) *l, list_iter(T) *i)
 *		returns an iterator to list element after i in list l.
 *
 *	T list_iter_data(list(T) *l, list_iter(T) *i)
 *		returns the data associated with list iterator i.
 *
 *	list_foreach(list(T) *l, void (*fn)(T*, void*), void *extra)
 * 		for each list element, calls fn with a pointer to the elements
 * 		data and the extra argument. Returns nothing.
 *
 * License:
 * --------
 * 
 * Copyright (c) 2016 Gunnar Zötl <gz@tset.de>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef generic_list_h
#define generic_list_h

typedef unsigned long list_size_t;

struct _generic_list_iter {
	struct _generic_list_iter *next;
	char data[0];
};

struct _generic_list {
	list_size_t itemsize, nitems;
	struct _generic_list_iter *first, *last;
};

/* types
 */
#define list(T) struct _list_##T
#define list_iter(T) struct _list_iter_##T
#define list_iter_of(l) typeof(*(l)->first)

/* type definition
 */
#define define_list_type(T) list(T) { list_size_t itemsize, nitems; list_iter(T) { list_iter(T) *next; T data; } *first, *last; }

/* list(T) *list_new(T)
 */
static inline struct _generic_list *_list_new(list_size_t itemsize)
{
	struct _generic_list *res = calloc(1, sizeof(struct _generic_list));
	res->itemsize = itemsize;
	return res;
}
#define list_new(T) ((list(T)*) _list_new(sizeof(T)))

/* void list_free(list(T) *l)
 */
static inline void _list_free(struct _generic_list *l)
{
	struct _generic_list_iter *this = l->first, *next;
	while (this) {
		next = this->next;
		free(this);
		this = next;
	}
	free(l);
}
#define list_free(l) _list_free((struct _generic_list*)(l))

#define list_length(l) ((l)->nitems)

static inline struct _generic_list_iter *_generic_list_iter_new(const void *v, list_size_t s)
{
	struct _generic_list_iter *item = malloc(sizeof(struct _generic_list_iter) + s);
	if (!item) { return 0; }
	memcpy(&item->data, v, s);
	return item;
}

/* int list_prepend(list(T) *l, T v)
 */
static inline int _list_prepend(struct _generic_list *l, const void *v)
{
	struct _generic_list_iter *item = _generic_list_iter_new(v, l->itemsize);
	item->next = l->first;
	l->first = item;
	l->nitems += 1;
	if (!l->last) {
		l->last = item;
	}
	return 1;
}
#define list_prepend(l, v) _list_prepend((struct _generic_list*)(l), (v))

/* int list_append(list(T) *l, T v)
 */
static inline int _list_append(struct _generic_list *l, const void *v)
{
	if (!l->last) {
		return _list_prepend(l, v);
	}
	struct _generic_list_iter *item = _generic_list_iter_new(v, l->itemsize);
	l->last->next = item;
	item->next = 0;
	l->last = item;
	l->nitems += 1;
	return 1;
}
#define list_append(l, v) _list_append((struct _generic_list*)(l), (v))

/* list_iter(l) *list_item(list(T) *l, list_size_t pos)
 */
static inline struct _generic_list_iter *_list_item(struct _generic_list *l, list_size_t pos)
{
	if (pos > 1 && pos < l->nitems) {
		struct _generic_list_iter *m = l->first;
		list_size_t i = 1;
		while (i < pos) {
			m = m->next;
			i += 1;
		}
		return m;
	} else if (pos == 1) {
		return l->first;
	} else if (pos == l->nitems) {
		return l->last;
	}
	return 0;
}
#define list_item(l, p) ((list_iter_of(l)*) _list_item((struct _generic_list*)(l), (p)))

/* int list_insert_at(list(T) *l, list_size_t p, T v)
 */
static inline int _list_insert_at(struct _generic_list *l, list_size_t p, const void *v)
{
	if (p < 1 || p > l->nitems) {
		return 0;
	}
	if (p == 1) {
		return _list_prepend(l, v);
	}
	struct _generic_list_iter *item = _generic_list_iter_new(v, l->itemsize);
	struct _generic_list_iter *m = _list_item(l, p - 1);
	item->next = m->next;
	m->next = item;
	l->nitems += 1;
	return 1;
}
#define list_insert_at(l, p, v) _list_insert_at((struct _generic_list*)(l), (p), (v))

/* int list_insert_after(list(T) *l, list_iter(T) *i, T v)
 */
static inline int _list_insert_after(struct _generic_list *l, struct _generic_list_iter *i, const void *v)
{
	if (!i) {
		return 0;
	}
	struct _generic_list_iter *item = _generic_list_iter_new(v, l->itemsize);
	item->next = i->next;
	i->next = item;
	l->nitems += 1;
	return 1;
}
#define list_insert_after(l, i, v) _list_insert_after((struct _generic_list*)(l), (struct _generic_list_iter*)(i), (v))


static inline void _list_remove_first(struct _generic_list *l)
{
	if (l->first) {
		struct _generic_list_iter *item = l->first;
		l->first = l->first->next;
		l->nitems -= 1;
		if (l->last == item) {
			l->last = l->first;
		}
		free(item); // TODO i->data?
	}
}

static inline void _list_remove_last(struct _generic_list *l)
{
	if (l->first == l->last) {
		_list_remove_first(l);
	} else {
		struct _generic_list_iter *item = l->first;
		while (item->next && item->next != l->last) {
			item = item->next;
		}
		if (item->next) {
			free(item->next); // TODO i->data?
			item->next = 0;
			l->last = item;
			l->nitems -= 1;
		}
	}
}

/* void list_remove_at(list(T) *l, list_size_t p)
 */
static inline void _list_remove_at(struct _generic_list *l, list_size_t p)
{
	if (p < 1 || p > l->nitems) {
		return;
	}
	if (p == 1) {
		_list_remove_first(l);
	} else if (p == l->nitems) {
		_list_remove_last(l);
	} else {
		struct _generic_list_iter *item = _list_item(l, p - 1);
		if (item) {
			struct _generic_list_iter *next = item->next;
			item->next = next->next;
			free(next); // TODO i->data?
			l->nitems -= 1;
		}
	}
}
#define list_remove_at(l, p)  _list_remove_at((struct _generic_list*)(l), (p))

/* void list_remove(list(T) *l, list_iter(T) *i)
 */
static inline void _list_remove(struct _generic_list *l, struct _generic_list_iter *i)
{
	if (i == l->first) {
		return _list_remove_first(l);
	} else if (i == l->last) {
		return _list_remove_last(l);
	} else {
		struct _generic_list_iter *item = l->first;
		while (item->next && item->next != i) {
			item = item->next;
		}
		if (!item->next) {
			return;
		}
		item->next = i->next;
		if (i == l->last) {
			l->last = item;
		}
		free(i); // TODO i->data?
		l->nitems -= 1;
	}
}
#define list_remove(l, i) _list_remove((struct _generic_list*)(l), (struct _generic_list_iter*)(i))

/* list_iter(l) *list_first(list(T) *l)
 */
#define list_first(l) ((l)->first)

/* list_iter(l) *list_last(list(T) *l)
 */
#define list_last(l) ((l)->last)

/* list_iter(l) *list_end(list(T) *l)
 */
#define list_end(l) (0)

/* list_iter(T) *list_iter_next(list(T) *l, list_iter(T) *i)
 */
#define list_iter_next(l, i) ((i)->next)

/* T list_iter_data(list(T) *l, list_iter(T) *i)
 */
#define list_iter_data(l, i) ((i)->data)

/* list_foreach(list(T) *l, void (*fn)(T*, void*), void *extra)
 */
#define list_foreach(l, fn, extra) do { list_iter_of(l) *___l01 = list_first(l); while (___l01) { fn(&___l01->data, extra); ___l01 = ___l01->next; } } while (0)

#endif /* generic_list_h */
