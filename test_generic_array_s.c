/* gcc -o test_generic_array_s test_generic_array_s.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "generic_array.h"
#include "generic_array_s.h"

#define TEST_SIZE 10

define_array_type(int);

int intcmp(const void *i1p, const void *i2p)
{
	int i1 = *(int*)i1p;
	int i2 = *(int*)i2p;
	return i1 == i2 ? 0 : i1 < i2 ? -1 : 1;
}

int main(int argc, char **argv)
{
	array(int) *ia = array_new(int, 8);
	int v, i, k, ok;

	srand(17);
	for (i = 0; i < TEST_SIZE; ++i) {
		v = rand();
		ok = array_insert_s(ia, &v, intcmp);
		if (ok < 0) i--;
	}

	if (array_size(ia) == TEST_SIZE) {
		printf("Array size ok\n");
	} else {
		printf("Array size failed\n");
		exit(1);
	}

	for (i = 1; i < TEST_SIZE; ++i) {
		if (array_data(ia)[i-1] >= array_data(ia)[i]) {
			printf("Array sortedness failed.\n");
			exit(1);
		}
	}
	printf("Array sortedness ok.\n");

	for (i = 0; i < TEST_SIZE; ++i) {
		int v = array_data(ia)[i];
		if (array_find_s(ia, &v, intcmp) != i) {
			printf("Array find failed.\n");
			exit(1);
		}
	}	
	printf("Array find ok.\n");

	for (i = 0; i < TEST_SIZE; ++i) {
		int p = rand() % array_size(ia);
		int v = array_data(ia)[p];
		ok = array_remove_s(ia, &v, intcmp);
		if (ok == -1) {
			printf("Array remove failed.\n");
			exit(1);
		}
		for (k = 0; k < array_size(ia); ++k) {
			if (array_data(ia)[k] == v) {
				printf("Array remove failed.\n");
				exit(1);
			}
		}

	}	
	printf("Array remove ok.\n");

	return 0;
}
