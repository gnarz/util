/* generic_array.h
 * 
 * Gunnar Zötl <gz@tset.de> 2014-2016
 * 
 * generic resizable arrays for C, somewhat type safe. Note: no attempt
 * is made to clean up the data contained in the array items, you need to
 * do this yourself.
 * 
 * Stuff starting with _array_* is reserved and private. Don't name your
 * identifiers like that, and don't use stuff named like that. Also, in
 * order to achieve some kind of macro hygiene, local variables in macros
 * are named ___aXX, where XX is a number. Don't use those names either.
 *
 * All functions are actually macros, that may call functions. The macros
 * take care of any type conversions needed.
 *
 * Needed #includes:
 * -----------------
 * 
 * 	<stdlib.h> <string.h>
 * 
 * Type creator:
 * -------------
 * 
 *	<struct> define_array_type(T)
 *		creates a struct for an array with element type T
 *		use: define_array(T);
 *			 array(T) the_array = array_new(T);
 * 		Note: Type T must be a single word, so if you have pointers or
 * 		structs, you need to typedef them first.
 * 
 * Types:
 * ------
 *
 *	array(T)
 *		type for an array with element type T.
 *
 *	array_iter(T)
 *		type for an array(T) iterator
 *
 *	array_iter_of(array(T) *a)
 *		type of the iterator for a given array
 *
 * Functions:
 * ----------
 * 
 *	size_t array_size(array(T) *a)
 * 		returns the size of an array
 * 
 *	size_t array_capacity(array(T) *a)
 * 		returns the capacity (i.e. currently allocated n.o.
 * 		elements) of an array. This is always equal to or greater than
 * 		the arrays size.
 *
 *	T* array_data(array(T) *a)
 * 		returns a pointer to the first element of the array, to
 * 		be directly indexed.
 * 
 * 	array(T)* array_new(T, int prealloc)
 * 		create a new array with elements of type T and prealloc
 * 		elements preallocated. Returns the newly allocated array.
 * 
 * 	void array_free(array(T) *a)
 * 		delete an array
 * 
 *	array_iter(T) *array_first(array(T) *a)
 * 		returns an iterator to the first element of the array, or 0 on
 * 		failure.
 * 
 *	array_iter(T) *array_last(array(T) *a)
 * 		returns an iterator to the last element of the array, or 0 on
 * 		failure.
 * 
 *	array_iter(T) *array_end(array(T) *a)
 * 		returns an iterator into a such that array_next(array_last(l)) == array_end(l),
 *		or 0 on failure.
 * 
 *	array_iter(T) *array_iter_next(array(T) *a, array_iter(T) *i)
 * 		returns an iterator to the next element in the array after the one
 * 		pointed to by ptr (which must be an array element pointer), or
 * 		0 on failure.
 * 
 *	T array_iter_data(array(T) *a, array_iter(T) *i)
 *		returns the value stored in an array iterator
 *
 * 	void array_foreach(array(T) *a, void (*fn)(T*, void*), void *extra)
 * 		for each array item, calls fn with a pointer to the array item
 * 		data and the extra argument. Returns nothing.
 * 
 * 	int array_resize(array(T) *a, size_t s)
 * 		grow or shrink an array to the new capacity s. Returns 1 on
 * 		success, 0 on failure.
 * 
 *	int array_ensure(array(T) *a, size_t nfree)
 * 		make sure that the capacity of the array is at least nfree elements
 * 		greater than its size, resizing the array if necessary. Returns 1
 *		on success, 0 on failure.
 * 
 *	int array_insert(array(T) *a, size_t pos, size_t nelem)
 * 		makes room for nelem elements at position pos into the array,
 * 		shifting the elements after pos upwards and growing the array
 * 		capacity, if necessary. The inserted elements remain
 * 		uninitialized. It is an error if pos > array_size(a).
 * 		Returns 1 on success, 0 on failure.
 * 
 *	int array_append(array(T) *a, const T *data)
 * 		append a value after the last value of an array. data is the
 * 		pointer to the value to append.
 * 		Returns 1 on success, 0 on failure.
 * 
 *	int array_remove(array(T) *a, size_t pos, size_t nelem)
 * 		removed nelem elements from the array, starting from position
 * 		pos, and shifting later elements down. The capacity remains
 * 		unchanged. It is an error if pos + nelem > array_size(a) or
 * 		pos > array_size(a).
 * 		Returns 1 on success, 0 on failure.
 * 
 * License:
 * --------
 * 
 * Copyright (c) 2014-2016 Gunnar Zötl <gz@tset.de>
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef _generic_array_h
#define _generic_array_h

#define ARRAY_SEQUENTIAL_GROW_BY 8

struct _generic_array_header {
	unsigned int itemsize, growby;
	unsigned int nitems, nalloc;
};

struct _generic_array {
	struct _generic_array_header header;
	void* data;
};

/* types
 */
#define array(T) struct _array_##T
#define array_iter(T) T
#define array_iter_of(a) typeof(*(a)->data)

/* type definition
 */
#define define_array_type(T) array(T) { struct _generic_array_header header; array_iter(T)* data; }

/* int array_size(a)
 */
#define array_size(a) ((a)->header.nitems)

/* int array_capacity(a)
 */
#define array_capacity(a) ((a)->header.nalloc)

/* T* array_data(a)
 */
#define array_data(a) ((a)->data)

/* array(T)* array_new(T, prealloc, ci)
 */
static inline struct _generic_array *_array_new(size_t asize, size_t itemsize, size_t prealloc)
{
	struct _generic_array *res = calloc(1, asize);
	if (res) {
		res->header.growby = ARRAY_SEQUENTIAL_GROW_BY;
		res->header.itemsize = itemsize;
		res->header.nitems = 0;
		if (prealloc > 0) {
			res->data = malloc(prealloc * itemsize);
			if (res->data)
				res->header.nalloc = prealloc;
			else {
				free(res);
				res = 0;
			}
		} else {
			res->header.nalloc = 0;
			res->data = 0;
		}
	}
	return res;
}
#define array_new(T, prealloc) ((array(T)*) _array_new(sizeof(struct _array_##T), sizeof(T), (prealloc)))

/* array_set_growby(a, n)
 */
#define array_set_growby(a, n) ((a)->header.growby = n)

/* void array_free(a)
 */
static inline void _array_free(struct _generic_array *a)
{
	if (a) {
		if (a->data) free(a->data);
		free(a);
	}
}
#define array_free(a) _array_free((struct _generic_array*)(a))

/* array_iter(T) *array_first(a)
 */
#define array_first(a) array_data(a)

/* array_iter(T) *array_last(a)
 */
static inline void *_array_last(struct _generic_array *a)
{
	if (array_size(a) > 0)
		return &(((char*)(array_data(a)))[(array_size(a) - 1) * a->header.itemsize]);
	return 0;
}
#define array_last(a) ((array_iter_of(a)*)_array_last((struct _generic_array*)(a)))

/* array_iter(T) *array_end(a)
 */
static inline void *_array_end(struct _generic_array *a)
{
	if (array_size(a) > 0)
		return &(((char*)(array_data(a)))[array_size(a) * a->header.itemsize]);
	return 0;
}
#define array_end(a) ((array_iter_of(a)*)_array_end((struct _generic_array*)(a)))

/* array_iter(T) *array_iter_next(a, iter)
 */
static inline void *_array_iter_next(struct _generic_array *a, void *p)
{
	int diff = (void*)p - array_data(a);
	if (diff >= array_size(a) * a->header.itemsize)
		return NULL;
	return p + a->header.itemsize;
}
#define array_iter_next(a, p) ((array_iter_of(a)*)_array_iter_next((struct _generic_array*)(a), (void*)p))

/* T array_iter_data(a, iter)
 */
#define array_iter_data(a, i) (*(i))

/* void array_foreach(array(T) *a, void (*fn)(T*))
 */
#define array_foreach(a, fn, extra) do { typeof(a) ___a01 = a; array_iter_of(a)* ___a02; for (___a02 = array_first(___a01); ___a02 != array_end(___a01); ___a02 = array_iter_next(___a01, ___a02)) { fn(___a02, extra); } } while (0)

/* int array_resize(a, size)
 */
static inline int _array_resize(struct _generic_array *a, size_t size)
{
	if (size > 0) {
		void *ndata = realloc(array_data(a), size * a->header.itemsize);
		if (ndata) {
			array_capacity(a) = size;
			if (size < array_size(a)) array_size(a) = size;
			array_data(a) = ndata;
			return 1;
		}
	} else if (size == 0) {
		if (array_data(a)) free(array_data(a));
		array_data(a) = 0;
		array_capacity(a) = 0;
		array_size(a) = 0;
		return 1;
	}
	return 0;
}
#define array_resize(a, size) _array_resize((struct _generic_array*)(a), (size))

/* int array_ensure(a, nfree)
 */
static inline int _array_ensure(struct _generic_array *a, size_t nfree)
{
	if (array_capacity(a) - array_size(a) >= nfree)
		return 1;
	return array_resize(a, array_size(a) + (int)((nfree + a->header.growby - 1) / a->header.growby) * a->header.growby);
}
#define array_ensure(a, nfree) _array_ensure((struct _generic_array*)(a), (nfree))

/* int array_insert(a, pos, elem)
 */
static inline int _array_insert(struct _generic_array *a, size_t pos, size_t nelem)
{
	if ((pos > array_size(a)) || (array_ensure(a, nelem) == 0))
		return 0;
	array_size(a) += nelem;
	if (pos < array_size(a)) {
		memmove(&(((char*)(array_data(a)))[(pos + nelem) * a->header.itemsize]), &(((char*)(array_data(a)))[pos * a->header.itemsize]), (array_size(a) - nelem - pos) * a->header.itemsize);
	}
	return 1;
}
#define array_insert(a, pos, nelem) _array_insert((struct _generic_array*)(a), (pos), (nelem))

/* int array_append(a, elem)
 */
static inline int _array_append(struct _generic_array *a, const void* elem)
{
	int ok = array_ensure(a, 1);
	if (ok) {
		memcpy(&(((char*)(array_data(a)))[array_size(a) * a->header.itemsize]), elem, a->header.itemsize);
		array_size(a)++;
	}
	return ok;
}
#define array_append(a, elem) _array_append((struct _generic_array*)(a), (elem))

/* int array_remove(a, pos, nelem)
 */
static inline int _array_remove(struct _generic_array *a, size_t pos, size_t nelem)
{
	if (pos > array_size(a))
		return 0;
	if (pos + nelem > array_size(a))
		nelem = array_size(a) - pos;
	if (pos + nelem <= array_size(a)) {
		memmove(&(((char*)(array_data(a)))[pos * a->header.itemsize]), &(((char*)(array_data(a)))[(pos + nelem) * a->header.itemsize]), (array_size(a) - nelem - pos) * a->header.itemsize);
	}
	array_size(a) -= nelem;
	return 1;
}
#define array_remove(a, pos, nelem) _array_remove((struct _generic_array*)(a), (pos), (nelem))

#endif /* _generic_array_h */
