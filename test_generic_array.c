/* gcc -o test_generic_array test_generic_array.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "generic_array.h"

#define TESTSTRING "Dies ist ein Test."

define_array_type(char);
define_array_type(int);

static void printint(int i)
{
	printf("<%d>:", i);
}

static void printch(int c)
{
	if (c < ' ') {
		printf("\\%02X:", c);
	} else {
		printf("%c:", c);
	}
}

/* array_new(int)
 * array_append -> array_ensure -> array_resize to grow
 * array_first
 * array_end
 * array_iter_next
 * array_iter_data
 * array_size
 * array_capacity
 * array_free
 */
int test1(void)
{
	puts("--- Test 1");
	char *teststr = TESTSTRING, *q;
	array(char) *a = array_new(char, 0);
	array_iter(char)* p;
	int ok = 1;
	
	for (q = teststr; *q != 0; ++q)
		array_append(a, q);
	
	q = teststr;
	for (p = array_first(a); p != array_end(a); p = array_iter_next(a, p)) {
		printch(array_iter_data(a, p));
		ok &= (array_iter_data(a, p) == *q++);
	}

	puts("");
	printf("array size: %d\n", array_size(a));
	printf("array capacity: %d\n", array_capacity(a));

	puts(ok ? "--- passed." : "--- failed.");

	array_free(a);
	return ok;
}

/* array_new with struct type
 * array_append -> array_ensure -> array_resize to grow
 * array_first
 * array_end
 * array_iter_next
 * array_iter_data
 * array_size
 * array_capacity
 * array_free
 */
typedef struct {
	short i;
	char c;
} data2;

int test2(void)
{
	puts("--- Test 2");
	char *teststr = TESTSTRING, *c;
	data2 d, *p;
	int i = 0;
	define_array_type(data2);
	array(data2) *a = array_new(data2, 0);
	int ok = 1;
	
	for (c = teststr; *c != 0; ++c) {
		d.i = ++i;
		d.c = *c;
		array_append(a, &d);
	}
	
	i = 0; c = teststr;
	for (p = array_first(a); p != array_end(a); p = array_iter_next(a, p)) {
		printint(array_iter_data(a, p).i);
		printch(array_iter_data(a, p).c);
		ok &= (array_iter_data(a, p).i == ++i) && (array_iter_data(a, p).c == *c++);
	}
	
	puts("");
	printf("array size: %d\n", array_size(a));
	printf("array capacity: %d\n", array_capacity(a));

	puts(ok ? "--- passed." : "--- failed.");

	array_free(a);
	return ok;
}

/* array_new with simple type (char)
 * array_append -> array_ensure -> array_resize to grow
 * array_resize to shrink
 * array_first
 * array_end
 * array_iter_next
 * array_iter_data
 * array_size
 * array_capacity
 * array_free
 */
int test3(void)
{
	puts("--- Test 3");
	char *teststr = TESTSTRING, *q;

	array(char) *a = array_new(char, 0);
	array_iter(char)* c;
	int newlen = strlen(teststr) - 3;
	int ok = 1;
	
	for (q = teststr; *q != 0; ++q)
		array_append(a, q);

	array_resize(a, newlen);
	
	ok &= (array_size(a) == newlen) && (array_capacity(a) == newlen);
	
	q = teststr;
	for (c = array_first(a); c != array_end(a); c = array_iter_next(a, c)) {
		printch(array_iter_data(a, c));
		ok &= (array_iter_data(a, c) == *q++);
	}
	
	puts("");
	printf("array size: %d\n", array_size(a));
	printf("array capacity: %d\n", array_capacity(a));

	puts(ok ? "--- passed." : "--- failed.");

	array_free(a);
	return ok;
}

/* array_new with simple type (char)
 * array_append -> array_ensure -> array_resize to grow
 * array_insert
 * array_data
 * array_first
 * array_end
 * array_iter_next
 * array_iter_data
 * array_size
 * array_capacity
 * array_free
 */
int test4(void)
{
	puts("--- Test 4");
	char *teststr1 = "Hallo!", *teststr2 = "xyzzyx", *p, *q;
	int i, ok = 1;
	array(char) *a = array_new(char, 0);
	array_iter(char)* c;

	for (p = teststr1; *p != 0; ++p)
		array_append(a, p);
	
	i = 3;
	array_insert(a, i, strlen(teststr2));
	p = array_data(a);
	for (q = teststr2; *q != 0; ++q)
		p[i++] = *q;
	
	ok &= (array_size(a) == strlen(teststr1) + strlen(teststr2));
	ok &= (array_capacity(a) >= array_size(a));
	
	i = 0;
	p = teststr1; q = teststr2;
	for (c = array_first(a); c != array_end(a); c = array_iter_next(a, c)) {
		printch(array_iter_data(a, c));
		if (i < 3 || i >= 3 + strlen(teststr2))
			ok &= (array_iter_data(a, c) == *p++);
		else
			ok &= (array_iter_data(a, c) == *q++);
		++i;
	}
	
	puts("");
	printf("array size: %d\n", array_size(a));
	printf("array capacity: %d\n", array_capacity(a));

	puts(ok ? "--- passed." : "--- failed.");

	array_free(a);
	return ok;
}

/* array_new with simple type (char)
 * array_append -> array_ensure -> array_resize to grow
 * array_insert
 * array_remove
 * array_data
 * array_first
 * array_end
 * array_iter_next
 * array_size
 * array_capacity
 * array_free
 */
int test5(void)
{
	puts("--- Test 5");
	char *teststr1 = "Hallo!", *teststr2 = "xyzzyx", *p, *q;
	int i, ok = 1;
	array(char) *a = array_new(char, 0);
	array_iter(char)* c;

	for (p = teststr1; *p != 0; ++p)
		array_append(a, p);
	
	i = 3;
	ok &= array_insert(a, i, strlen(teststr2));
	p = array_data(a);
	for (q = teststr2; *q != 0; ++q)
		p[i++] = *q;
	
	ok &= (array_size(a) == strlen(teststr1) + strlen(teststr2));
	ok &= (array_capacity(a) >= array_size(a));
	
	ok &= array_remove(a, 3, strlen(teststr2));
	
	p = teststr1;
	for (c = array_first(a); c != array_end(a); c = array_iter_next(a, c)) {
		printch(array_iter_data(a, c));
		ok &= (array_iter_data(a, c) == *p++);
	}
	
	ok &= (array_size(a) == strlen(teststr1));
	
	puts("");
	printf("array size: %d\n", array_size(a));
	printf("array capacity: %d\n", array_capacity(a));

	puts(ok ? "--- passed." : "--- failed.");

	array_free(a);
	return ok;
}

/* array_new with simple type(int)
 * array_append
 * array_foreach
 * array_free
 * direct access with array_data
 */
void test6_sum(int *i, void *xsum)
{
	int *sum = (int*) xsum;
	*sum += *i;
}

int test6(void)
{
	puts("--- Test 6");
	int i, ok = 1, sum = 0, osum = 0, dsum = 0;
	int *ap;
	array(int) *a = array_new(int, 0);
	
	for (i = 1; i <= 101; ++i) {
		array_append(a, &i);
		osum += i;
	}
	
	array_foreach(a, test6_sum, (void*) &sum);
	
	ok = sum == osum;
	
	ap = array_data(a);
	for (i = 0; i < array_size(a); ++i)
		dsum += ap[i];
	
	ok &= sum == dsum;
	
	printf("array size: %d\n", array_size(a));
	printf("array capacity: %d\n", array_capacity(a));

	puts(ok ? "--- passed." : "--- failed.");

	array_free(a);
	return ok;
}

int main(int argc, char **argv)
{
	test1();
	test2();
	test3();
	test4();
	test5();
	test6();
	
	return 0;
}
