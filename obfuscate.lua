#!/usr/bin/env lua
--[[
 	obfuscate html code (utf-8 encoded!), outputs obfuscated stuff with
	decoder routine. This is not an encoding, which would not make sense
	anyway... it is just to protect e-mail addresses and the like from
	malicious crawlers.
--]]

local sdata

if #arg == 1 then
	local file = arg[1]
	local f = io.open(file, "r")
	if not f then
		io.stderr:write(string.format("could not open file %s for input\n", file))
		os.exit(1)
	end
	sdata = f:read("*a")
	f:close()
elseif #arg == 2 and arg[1] == "-s" then
	sdata = arg[2]
else
	io.stderr:write("Usage: obfuscate.lua file\n   or: obfuscate.lua -s string\n")
	os.exit(1)
end

local data = { utf8.codepoint(sdata, 1, -1) }
local enc = {}

local j = 1
for i=1, #data do
	if data[i] < 128 then
		enc[j] = string.format("%02X", data[i])
		j = j + 1
	else
		local ls = string.format("&#x%X;", data[i])
		local ld = { string.byte(ls, 1, -1) }
		for k=1, #ld do
			enc[j] = string.format("%02X", ld[k])
			j = j + 1
		end
	end
end

io.write('<script language="javascript">try{if(window&&document){var s="')
io.write(table.concat(enc))
io.write('",i,c;for(i=0;i<s.length;i+=2){')
io.write('document.write(String.fromCharCode(parseInt("0x"+s.substr(i,2))));')
io.write('}}}catch(e){}</script>\n')